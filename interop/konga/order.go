package konga

import (
	"strconv"
	"fmt"
	"time"
	"bitbucket.org/laverita/interecommerce/db"
	"bitbucket.org/laverita/interecommerce/domain"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"bitbucket.org/laverita/interecommerce/services"
)

type (
	OrderItem struct {
		ItemId 		string  `json:"item_id"`
		ProductTitle	string	`json:"product_name"`
		ProductSku	string	`json:"product_sku"`
		// CreatedDate	time.Time `json:"created_date"`
		Price 		float64	`json:"price,string"`
		Quantity 	uint	`json:"quantity,string"`

	}

	OrderDto	struct {
		EntityId      string        	`json:"entity_id"`
		OrderState    string		`json:"order_state"`
		Status        string		`json:"status"`
		SubTotal      float64		`json:"subtotal,string"`
		GrandTotal    float64		`json:"grand_total,string"`
		PaymentType   string		`json:"payment_type"`
		ShippingPrice float64		`json:"shipping_price,string"`
		OrderItems    []OrderItem	`json:"order_items"`
	}
	FetchOrderDataDto struct {
		Orders     []OrderDto 	`json:"list"`
		TotalCount int 		`json:"total_count,string"`
		Count      int 		`json:"count"`
		Page       PageDto	`json:"page"`
	}

	FetchOrdersResponse struct {
		Status 	string 	`json:"status"`
		Code        string	`json:"code"`
		Message     string	`json:"message"`
		Data        FetchOrderDataDto `json:"data"`
	}
)

const (
	STATUS_ORDER_NEW = "accepted"
	STATUS_ORDER_SHIPPED = "shipped"
	STATUS_ORDER_CANCELLED = "canceled"
	STATUS_ORDER_RETURNED = "returned"
	STATUS_ORDER_DELIVERED = "delivered"
	STATUS_ORDER_ARBITRATION = "arbitration"


)
var StatusMap map[string]uint = map[string]uint{
	STATUS_ORDER_NEW:domain.StatusOrderNew,
	STATUS_ORDER_SHIPPED: domain.StatusOrderShipped,
	STATUS_ORDER_CANCELLED: domain.StatusOrderCancelled,
	STATUS_ORDER_RETURNED: domain.StatusOrderReturned,
	STATUS_ORDER_DELIVERED: domain.StatusOrderDelivered,
}

func (o OrderItem) GetOrCreateProduct() (product domain.Product) {
	_db := db.GetDb()
	err := _db.Where("name = ? OR sku = ?", o.ProductTitle, o.ProductSku).Error
	if err != nil {
		product.Name = o.ProductTitle
		product.Sku = o.ProductSku
		_db.Create(&product)
	}
	return
}


func (r FetchOrdersResponse) IsSuccess() bool {
	return r.Status == "success"
}


func ProcessOrders() {
	var page int
	var count int

	for {
		_url := `https://shq.konga.com/ajax/shqmw/v3/merchants/5723/orders/?fields=*,order_items.*&embed=order_items,parent_order&page=` +
			strconv.Itoa(page) + `&order_by=t.created_date%20DESC&count=50`
		content :=  request(_url)
		var response FetchOrdersResponse
		err := parseResponse(content, &response)

		if err == nil && response.IsSuccess() {
			for _, kongaOrder := range response.Data.Orders {
				count++
				fmt.Printf("Processing Order No: %d\n", count)
				if kongaOrder.SubTotal == 0 {
					continue
				}
				_db := db.GetDb().Begin()
				switch kongaOrder.OrderState {
				default:
					fmt.Printf("Unknown status: %s\n\n", kongaOrder.OrderState)
					continue
				case STATUS_ORDER_NEW:
					_, err = createOrder(kongaOrder, _db)
					if err != nil {
						fmt.Printf("create err : %v", err)
					}
				case STATUS_ORDER_SHIPPED:
					_, err = processShippedOrders(kongaOrder, _db)
					if err != nil {
						fmt.Printf("shipped err : %v", err)
					}
				case STATUS_ORDER_CANCELLED:
					_, err = processCancelledOrders(kongaOrder, _db)
					if err != nil {
						fmt.Printf("canceled err : %v", err)
					}
				case STATUS_ORDER_DELIVERED:
					_, err = processDeliveredOrders(kongaOrder, _db)
					if err != nil {
						fmt.Printf("delivered err : %v", err)
					}
				case STATUS_ORDER_RETURNED:
					_, err = processReturnedOrder(kongaOrder, _db)
					if err != nil {
						fmt.Printf("returned err : %v", err)
					}
				}

				if err == nil {
					_db.Commit()
				}else {
					_db.Rollback()
				}

			}
			if count == response.Data.TotalCount {
				return
			}
			page++
			continue
		}

		fmt.Println(response.Message)
		fmt.Printf("Err: %v", err)
		return

	}
}

func getOrderHistory(orderId, statusId uint, _db *gorm.DB) (history domain.OrderHistory, err error) {
	err = _db.Where("order_id = ? AND status_id = ?", orderId, statusId).Find(&history).Error
	return
}


func createOrder(kongaOrder OrderDto, _db *gorm.DB) (order domain.Order, err error) {
	if err = _db.Where("market_id = ? AND ref_id = ?", domain.MARKET_KONGA, kongaOrder.EntityId).Find(&order).Error; err == nil {
		return // order exists
	}
	if kongaOrder.SubTotal < 1  || kongaOrder.GrandTotal < 1  || len(kongaOrder.OrderItems) == 0 {
		err = errors.New("Cannot create an empty order")
		return
	}
	var statusId uint
	var ok bool
	if statusId, ok = StatusMap[kongaOrder.OrderState]; !ok {
		statusId = domain.StatusUnknown
	}
	order = domain.Order{
		RefId:kongaOrder.EntityId,
		MarketId:domain.MARKET_KONGA,
		StatusId: statusId,

	}
	for _, item := range kongaOrder.OrderItems {
		product := item.GetOrCreateProduct()
		order.Items = append(order.Items, domain.OrderItem{
			Quantity:item.Quantity,
			ProductId:product.ID,
			UnitPrice:item.Price,
		})
	}
	err = _db.Create(&order).Error
	if err != nil {
		if statusId == domain.StatusOrderNew {
			_, err = services.CreateOrderHistory(order.ID, domain.StatusOrderNew, _db)
		}
	}
	return
}

func processShippedOrders(kongaOrder OrderDto, _db *gorm.DB) (order domain.Order, err error) {
	if err = _db.Where("market_id = ? AND ref_id = ?", domain.MARKET_KONGA, kongaOrder.EntityId).Find(&order).Error; err != nil {
		order, err = createOrder(kongaOrder, _db)
		if err != nil {
			return
		}
	}else {
		if order.StatusId == domain.StatusOrderNew {
			return
		}
	}
	order.StatusId = domain.StatusOrderShipped
	if err = _db.Save(&order).Error; err != nil {
		_, err = services.CreateOrderHistory(order.ID, domain.StatusOrderShipped, _db)
		return
	}

	for _, item := range kongaOrder.OrderItems {
		product := item.GetOrCreateProduct()
		var stock domain.Stock
		if _db.Where("product_id = ?", product.ID).Find(&stock).Error != nil {
			stock.ProductId = product.ID
			_db.Create(stock)
		}
		stock.Quantity -= item.Quantity
		_db.Save(&stock)
	}

	return
}

func processCancelledOrders(kongaOrder OrderDto, _db *gorm.DB) (order domain.Order, err error) {
	if err = _db.Where("market_id = ? AND ref_id = ?", domain.MARKET_KONGA, kongaOrder.EntityId).Find(&order).Error; err != nil {
		order, err = createOrder(kongaOrder, _db)
		if err != nil {
			return
		}
	}else if order.StatusId == domain.StatusOrderCancelled {
		return
	}
	order.StatusId = domain.StatusOrderCancelled
	if err = _db.Save(&order).Error; err != nil {
		_, err = services.CreateOrderHistory(order.ID, domain.StatusOrderCancelled, _db)
		return
	}

	//TODO find out if a shipped or delivered order can be cancelled
	return
}

func processDeliveredOrders(kongaOrder OrderDto, _db *gorm.DB) (order domain.Order, err error) {
	//if the order does not exists, create one
	if err = _db.Where("market_id = ? AND ref_id = ?", domain.MARKET_KONGA, kongaOrder.EntityId).Find(&order).Error; err != nil {
		order, err = createOrder(kongaOrder, _db)
		if err != nil {
			return
		}
	}else if order.StatusId == domain.StatusOrderDelivered { // if order already deliver, return
		 return
	}else if order.StatusId != domain.StatusOrderShipped { // if the order has not been shipped, ship
		order, err = processShippedOrders(kongaOrder, _db)
		if err != nil {
			return
		}
	}

	if order.ID == 0 {
		return
	}

	order.StatusId = domain.StatusOrderDelivered
	if err = _db.Save(&order).Error; err != nil {
		_, err = services.CreateOrderHistory(order.ID, domain.StatusOrderDelivered, _db)
		return
	}

	sale := domain.Sale{
		OrderId:order.ID,
		MarketId:order.MarketId,
		Name:order.Display(),
		Date:time.Now(),
		StatusID:domain.StatusActive,
		Amount:kongaOrder.SubTotal,
	}

	/*for _, item := range kongaOrder.OrderItems {
		product := item.GetOrCreateProduct()
		sale.Amount += float64(item.Quantity) * product.Price
	}*/
	if sale.Amount < 1 || sale.OrderId == 0 {
		err = errors.New("Cannot create an empty sale")
		return
	}

	err = _db.Save(&sale).Error
	if err != nil {
		return
	}
	payment := domain.Payment{
		Amount:sale.Amount,
		SaleId:sale.ID,
		StatusId:domain.PaymentStatusPending,
	}

	err = _db.Create(&payment).Error
	return
}

func processReturnedOrder(kongaOrder OrderDto, _db *gorm.DB) (order domain.Order, err error) {
	var history domain.OrderHistory
	if err = _db.Where("market_id = ? AND ref_id = ?", domain.MARKET_KONGA, kongaOrder.EntityId).Find(&order).Error; err != nil {
		order, err = createOrder(kongaOrder, _db)
		if err != nil {
			return
		}
	}else if order.StatusId == domain.StatusOrderReturned {
		return
	}
	//todo move to receive cancelled order
	/*if history, err = getOrderHistory(order.ID, domain.StatusOrderShipped, _db); err != nil {
		var items []OrderItem
		if _db.Where("order_id = ?", order.ID).Find(&items	).Error != nil {
			for _, item := range items {
				var stock domain.Stock
				if _db.Where("product_id = ?").Find(&stock). Error != nil {
					stock.Quantity -= item.Quantity
					_db.Save(&stock)
				}
			}
		}
	}*/

	if history, err = getOrderHistory(order.ID, domain.StatusOrderDelivered, _db); err != nil {
		var sale domain.Sale
		var payment domain.Payment

		if _db.Where("order_id = ?", order.ID).Find(&sale).Error != nil {
			sale.StatusID = domain.StatusOrderReturned
			_db.Save(&sale)
			if _db.Where("sale_id = ?", sale.ID).Find(&payment).Error != nil {
				payment.StatusId = domain.StatusOrderReturned
				_db.Save(&payment)
			}
		}
	}
	order.StatusId = domain.StatusOrderReturned
	if err = _db.Save(&order).Error; err != nil {
		history = domain.OrderHistory{StatusId:order.StatusId, OrderId: order.ID,}
		_db.Create(&history)
		return
	}

	//TODO find out if a shipped or delivered order can be cancelled
	return
}
