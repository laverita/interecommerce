package konga

import (
	"bitbucket.org/laverita/interecommerce/interop"
	"net/http"
	"net/url"
	"github.com/PuerkitoBio/goquery"
	"log"
	"encoding/json"
)

type (
	Response struct {
		Status 	string 	`json:"status"`
		Code 	string	`json:"code"`
		Message string	`json:"message"`
		Data 	interface{} `json:"data"`
	}

	PageDto struct {
		Prev int `json:"prev"`
		Next int `json:"next"`
	}

)


func parseResponse(content string, receiver interface{}) (error) {
	err := json.Unmarshal([]byte(content), &receiver)
	return err
}

func (r Response) IsSuccess() bool {
	return r.Status == "success"
}


func getClient() (*http.Client, string, error) {
	// create the client
	client := interop.NewJarClient()

	// get the csrf token
	req, _ := http.NewRequest("GET", "https://shq.konga.com/account/login", nil)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		log.Fatal(err)
	}

	csrfToken := ""

	doc.Find("input").Each(func(index int, item *goquery.Selection) {
		if item.AttrOr("name","") == "__csrf_token" {
			csrfToken = string(item.AttrOr("value", ""))
			return
		}
	})

	// post on the login form.
	resp, _ = client.PostForm("https://shq.konga.com/account/login", url.Values{
		"User[email]":    {"obymekk4love@yahoo.com"},
		"User[password]": {"2206success"},
		"__csrf_token":          {csrfToken},
		"User[rememberMe]": {"0"},
		"login": {"LOGIN"},
	})

	//todo check for login failure
	/*doc, err = goquery.NewDocumentFromResponse(resp)
	if err != nil {
		log.Fatal(err)
	}

	html := doc.Text()

	response, err := parseResponse(html)
	if err != nil || !response.IsSuccess() {
		return nil, "", err
	}*/
	return client, csrfToken, nil
}

func request(_url string) (content string) {
	client, csrfToken, err := getClient()
	if err != nil {
		return
	}
	//fmt.Printf("CSRF: %s \n", csrfToken)
	_url += "&__csrf_token=" + csrfToken
	req, _ := http.NewRequest("GET", _url, nil)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		log.Fatal(err)
	}

	return doc.Text()
}

