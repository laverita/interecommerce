package konga

import (
	"fmt"
	"strconv"
	"bitbucket.org/laverita/interecommerce/domain"
	"bitbucket.org/laverita/interecommerce/db"
)

type (
	ProductDto struct {
		Name string 	`json:"name"`
		EntityId string `json:"entity_id"`
		Price float64	`json:"price,string"`
		Quantity uint 	`json:"qty,string"`
		Sku string	`json:"sku"`

	}

	FetchProductDataDto struct {
		Products 	[]ProductDto 	`json:"list"`
		TotalCount	int 		`json:"total_count,string"`
		Count 		int 		`json:"count"`
		Page 		PageDto		`json:"page"`
	}

	FetchProductsResponse struct {
		Status 	string 	`json:"status"`
		Code 	string	`json:"code"`
		Message string	`json:"message"`
		Data 	FetchProductDataDto `json:"data"`
	}
)


func (r FetchProductsResponse) IsSuccess() bool {
	return r.Status == "success"
}


func GetKongaProducts() {
	var page int
	var count int
	for {
		fmt.Printf("Fetching products page : %d\n", page)
		_url := "https://shq.konga.com/ajax/shqmw/v3/merchants/5723/products/?fields=*,attr.*,main_image.*,commission.*,total_qty,classification_id&filters=%5B%7B%22field%22:%22is_configurable%22,%22value%22:0%7D,%7B%22field%22:%22status%22,%22value%22:%22%3C%3E%20discard%22%7D%5D&embed=main_image,attr,commission&page=" + strconv.Itoa(page) + "&order_by=t.created_at%20desc"
		content :=  request(_url)
		var response FetchProductsResponse
		err := parseResponse(content, &response)

		if err != nil || !response.IsSuccess() {
			fmt.Println(response.Message)
			fmt.Printf("Err: %v", err)
			return
		}
		for _, kongaProduct := range response.Data.Products {
			count++
			fmt.Printf("Adding product No: %d", count)
			product := domain.Product{KongaId:kongaProduct.EntityId}
			_db := db.GetDb()
			if _db.Where("konga_id = ?", kongaProduct.EntityId).Find(&product).Error == nil {
				return
			}
			//todo add the product and register the stock info
			product.Name = kongaProduct.Name
			product.Sku = kongaProduct.Sku
			product.Price = kongaProduct.Price
			if _db.Save(&product).Error == nil {
				var stock domain.Stock
				stock.ProductId = product.ID
				stock.Quantity = kongaProduct.Quantity
				_db.Save(&stock)
			}
		}
		if count == response.Data.TotalCount {
			return
		}
		page++
	}
}
