package jumia

import (
	"net/http"
	"bitbucket.org/laverita/interecommerce/interop"
	"github.com/PuerkitoBio/goquery"
	"net/url"
	"github.com/labstack/gommon/log"
	"sort"
	"strings"
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
	"time"
	"bitbucket.org/laverita/interecommerce/app"
	"encoding/hex"
	"encoding/json"
)

type Product struct {
	Name 		string
	SellerSku 	string
	ShopSku 	string
	Quantity	int 		`json:"Quantity,string"`
	Price 		float64		`json:"Price,string"`
}

type Response struct {
	SuccessResponse struct {
		Head 	struct{}
		Body 	struct{
			Products 	struct{
				Product		[]Product
			}
		}
	}
	ErrorResponse	struct{
		Head 	struct{
			ErrorCode	int	`json:"ErrorCode,string"`
			ErrorMessage	string
		}
	}
}

func parseResponse(content string, receiver interface{}) (error) {
	err := json.Unmarshal([]byte(content), &receiver)
	return err
}

func getClient() (*http.Client,  error) {
	// create the client
	client := interop.NewJarClient()

	// post on the login form.
	client.PostForm("https://sellercenter.jumia.com.ng/user/auth/login", url.Values{
		"email":    {"obymekk4love@yahoo.com"},
		"password": {"2206Success"},
		"submit": {"Login"},
	})

	return client, nil
}

var apiBaseUrl = "http://localhost/jumia"

func computeHmac256(message string, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(message))

	return hex.EncodeToString(h.Sum(nil)) //base64.StdEncoding.EncodeToString(h.Sum(nil))
}

func urlencode(s string) (result string){
	//return s
	for _, c := range(s) {
		if c <= 0x7f { // single byte
			result += fmt.Sprintf("%%%X", c)
		} else if c > 0x1fffff {// quaternary byte
			result += fmt.Sprintf("%%%X%%%X%%%X%%%X",
				0xf0 + ((c & 0x1c0000) >> 18),
				0x80 + ((c & 0x3f000) >> 12),
				0x80 + ((c & 0xfc0) >> 6),
				0x80 + (c & 0x3f),
			)
		} else if c > 0x7ff { // triple byte
			result += fmt.Sprintf("%%%X%%%X%%%X",
				0xe0 + ((c & 0xf000) >> 12),
				0x80 + ((c & 0xfc0) >> 6),
				0x80 + (c & 0x3f),
			)
		} else { // double byte
			result += fmt.Sprintf("%%%X%%%X",
				0xc0 + ((c & 0x7c0) >> 6),
				0x80 + (c & 0x3f),
			)
		}
	}

	return result
}

func buildQueryString(action string, params map[string]string) string {
	email := "obymekk4love@yahoo.com"
	// 2006-01-02T15:14:15+00:00
	timeStamp := time.Now().Format("2006-01-02T15:04:05-0700")
	parameters := map[string]string{"UserID": email, "Version": "1.0", "Action": action,
		"Format": "JSON", "Timestamp": timeStamp}

	for key, value := range params {
		parameters[key] = value
	}
	var keys []string
	for key := range parameters {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	var encoded []string
	for _, key := range keys {
		encoded = append(encoded, key + "=" + parameters[key])
	}

	concatenated := strings.Join(encoded, "&")
	apiKey := "173b165b4adb8b1c16efe24ef9f77d2c7d5d9686"
	signature := computeHmac256(concatenated, apiKey)

	parameters["Signature"] = signature

	var parametersEncoded []string
	for key, value := range parameters {
		parametersEncoded = append(parametersEncoded, key + "="+ urlencode(value))
	}

	return strings.Join(parametersEncoded, "&")
}

func request(_url string) (content string) {
	client, err := getClient()
	if err != nil {
		return
	}
	//fmt.Printf("CSRF: %s \n", csrfToken)
	req, _ := http.NewRequest("GET", _url, nil)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		log.Fatal(err)
	}

	return doc.Text()
}

func LoadProducts() {
	var Url *url.URL
	Url, err := url.Parse(apiBaseUrl)
	if err != nil {
		panic(err)
	}

	queryString := buildQueryString("GetProducts", map[string]string{})
	Url.RawQuery = queryString

	app.Logger.Error(Url.String())


	var response Response
	err = getJson(Url.String(), &response)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%v", response)

}

var myClient = &http.Client{Timeout: 10 * time.Second}

func getJson(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}