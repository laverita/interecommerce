package main

import (
	_ "bitbucket.org/laverita/interecommerce/db/migrations"
	"bitbucket.org/laverita/interecommerce/app/common"
	"bitbucket.org/laverita/interecommerce/app/routers"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"bitbucket.org/laverita/interecommerce/db"
	"github.com/ccding/go-logging/logging"
	"bitbucket.org/laverita/interecommerce/app"
	"fmt"
	"bitbucket.org/superfluxteam/cruder"
	"bitbucket.org/laverita/interecommerce/app/crud"
	"bitbucket.org/laverita/interecommerce/app/controllers/pages"
	"net/http"
	"bitbucket.org/laverita/interecommerce/domain"
	"bitbucket.org/laverita/interecommerce/interop/jumia"
)

// Entry point of the program
func main() {
	//Calls startup logic
	common.StartUp()
	defer common.ShortDown()

	app.Logger, _ = logging.BasicLogger("main")
	defer app.Logger.Destroy()

	e := echo.New()
	// e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Allows requests from any origin TODO change this to frontend server domain
	e.Use(middleware.CORS())

	routers.InitRoutes(e)

	if app.Cruder == nil {
		app.Cruder = cruder.NewCruder(db.GetDb(), func(r *http.Request) cruder.CurrentUser {
			staff := domain.Staff{}
			staff.ID = 1
			return staff
		})
	}

	crud.InitCruderResources(app.Cruder)
	pages.InitPages(app.Cruder)

	app.Cruder.Mount("/api/v1/cruder", e)

	// go konga.GetKongaProducts()
	//go konga.ProcessOrders()
	go jumia.LoadProducts()
	//todo handle konga order -load order, check staus, update stock, sales
	// Start server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%s", common.AppConfig.Port)))

}


