package factory

import (
	"database/sql"
	"github.com/knq/dburl"
)

func GetConnection(dns string) *sql.DB {
	db, err := dburl.Open(dns)
	if err != nil{
		panic(err)
	}
	return db
}
