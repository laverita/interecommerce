// This file is generated by SQLBoiler (https://github.com/vattle/sqlboiler)
// and is meant to be re-generated in place and/or deleted at any time.
// DO NOT EDIT

package models

import (
	"bytes"
	"reflect"
	"testing"

	"github.com/vattle/sqlboiler/boil"
	"github.com/vattle/sqlboiler/randomize"
	"github.com/vattle/sqlboiler/strmangle"
)

func testPMSJobTypeForms(t *testing.T) {
	t.Parallel()

	query := PMSJobTypeForms(nil)

	if query.Query == nil {
		t.Error("expected a query, got nothing")
	}
}
func testPMSJobTypeFormsDelete(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	if err = pmsJobTypeForm.Delete(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testPMSJobTypeFormsQueryDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	if err = PMSJobTypeForms(tx).DeleteAll(); err != nil {
		t.Error(err)
	}

	count, err := PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testPMSJobTypeFormsSliceDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	slice := PMSJobTypeFormSlice{pmsJobTypeForm}

	if err = slice.DeleteAll(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}
func testPMSJobTypeFormsExists(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	e, err := PMSJobTypeFormExists(tx, pmsJobTypeForm.ID)
	if err != nil {
		t.Errorf("Unable to check if PMSJobTypeForm exists: %s", err)
	}
	if !e {
		t.Errorf("Expected PMSJobTypeFormExistsG to return true, but got false.")
	}
}
func testPMSJobTypeFormsFind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	pmsJobTypeFormFound, err := FindPMSJobTypeForm(tx, pmsJobTypeForm.ID)
	if err != nil {
		t.Error(err)
	}

	if pmsJobTypeFormFound == nil {
		t.Error("want a record, got nil")
	}
}
func testPMSJobTypeFormsBind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	if err = PMSJobTypeForms(tx).Bind(pmsJobTypeForm); err != nil {
		t.Error(err)
	}
}

func testPMSJobTypeFormsOne(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	if x, err := PMSJobTypeForms(tx).One(); err != nil {
		t.Error(err)
	} else if x == nil {
		t.Error("expected to get a non nil record")
	}
}

func testPMSJobTypeFormsAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeFormOne := &PMSJobTypeForm{}
	pmsJobTypeFormTwo := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeFormOne, pmsJobTypeFormDBTypes, false, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}
	if err = randomize.Struct(seed, pmsJobTypeFormTwo, pmsJobTypeFormDBTypes, false, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeFormOne.Insert(tx); err != nil {
		t.Error(err)
	}
	if err = pmsJobTypeFormTwo.Insert(tx); err != nil {
		t.Error(err)
	}

	slice, err := PMSJobTypeForms(tx).All()
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 2 {
		t.Error("want 2 records, got:", len(slice))
	}
}

func testPMSJobTypeFormsCount(t *testing.T) {
	t.Parallel()

	var err error
	seed := randomize.NewSeed()
	pmsJobTypeFormOne := &PMSJobTypeForm{}
	pmsJobTypeFormTwo := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeFormOne, pmsJobTypeFormDBTypes, false, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}
	if err = randomize.Struct(seed, pmsJobTypeFormTwo, pmsJobTypeFormDBTypes, false, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeFormOne.Insert(tx); err != nil {
		t.Error(err)
	}
	if err = pmsJobTypeFormTwo.Insert(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Error("want 2 records, got:", count)
	}
}
func pmsJobTypeFormBeforeInsertHook(e boil.Executor, o *PMSJobTypeForm) error {
	*o = PMSJobTypeForm{}
	return nil
}

func pmsJobTypeFormAfterInsertHook(e boil.Executor, o *PMSJobTypeForm) error {
	*o = PMSJobTypeForm{}
	return nil
}

func pmsJobTypeFormAfterSelectHook(e boil.Executor, o *PMSJobTypeForm) error {
	*o = PMSJobTypeForm{}
	return nil
}

func pmsJobTypeFormBeforeUpdateHook(e boil.Executor, o *PMSJobTypeForm) error {
	*o = PMSJobTypeForm{}
	return nil
}

func pmsJobTypeFormAfterUpdateHook(e boil.Executor, o *PMSJobTypeForm) error {
	*o = PMSJobTypeForm{}
	return nil
}

func pmsJobTypeFormBeforeDeleteHook(e boil.Executor, o *PMSJobTypeForm) error {
	*o = PMSJobTypeForm{}
	return nil
}

func pmsJobTypeFormAfterDeleteHook(e boil.Executor, o *PMSJobTypeForm) error {
	*o = PMSJobTypeForm{}
	return nil
}

func pmsJobTypeFormBeforeUpsertHook(e boil.Executor, o *PMSJobTypeForm) error {
	*o = PMSJobTypeForm{}
	return nil
}

func pmsJobTypeFormAfterUpsertHook(e boil.Executor, o *PMSJobTypeForm) error {
	*o = PMSJobTypeForm{}
	return nil
}

func testPMSJobTypeFormsHooks(t *testing.T) {
	t.Parallel()

	var err error

	empty := &PMSJobTypeForm{}
	o := &PMSJobTypeForm{}

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, o, pmsJobTypeFormDBTypes, false); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm object: %s", err)
	}

	AddPMSJobTypeFormHook(boil.BeforeInsertHook, pmsJobTypeFormBeforeInsertHook)
	if err = o.doBeforeInsertHooks(nil); err != nil {
		t.Errorf("Unable to execute doBeforeInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeInsertHook function to empty object, but got: %#v", o)
	}
	pmsJobTypeFormBeforeInsertHooks = []PMSJobTypeFormHook{}

	AddPMSJobTypeFormHook(boil.AfterInsertHook, pmsJobTypeFormAfterInsertHook)
	if err = o.doAfterInsertHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterInsertHook function to empty object, but got: %#v", o)
	}
	pmsJobTypeFormAfterInsertHooks = []PMSJobTypeFormHook{}

	AddPMSJobTypeFormHook(boil.AfterSelectHook, pmsJobTypeFormAfterSelectHook)
	if err = o.doAfterSelectHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterSelectHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterSelectHook function to empty object, but got: %#v", o)
	}
	pmsJobTypeFormAfterSelectHooks = []PMSJobTypeFormHook{}

	AddPMSJobTypeFormHook(boil.BeforeUpdateHook, pmsJobTypeFormBeforeUpdateHook)
	if err = o.doBeforeUpdateHooks(nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpdateHook function to empty object, but got: %#v", o)
	}
	pmsJobTypeFormBeforeUpdateHooks = []PMSJobTypeFormHook{}

	AddPMSJobTypeFormHook(boil.AfterUpdateHook, pmsJobTypeFormAfterUpdateHook)
	if err = o.doAfterUpdateHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpdateHook function to empty object, but got: %#v", o)
	}
	pmsJobTypeFormAfterUpdateHooks = []PMSJobTypeFormHook{}

	AddPMSJobTypeFormHook(boil.BeforeDeleteHook, pmsJobTypeFormBeforeDeleteHook)
	if err = o.doBeforeDeleteHooks(nil); err != nil {
		t.Errorf("Unable to execute doBeforeDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeDeleteHook function to empty object, but got: %#v", o)
	}
	pmsJobTypeFormBeforeDeleteHooks = []PMSJobTypeFormHook{}

	AddPMSJobTypeFormHook(boil.AfterDeleteHook, pmsJobTypeFormAfterDeleteHook)
	if err = o.doAfterDeleteHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterDeleteHook function to empty object, but got: %#v", o)
	}
	pmsJobTypeFormAfterDeleteHooks = []PMSJobTypeFormHook{}

	AddPMSJobTypeFormHook(boil.BeforeUpsertHook, pmsJobTypeFormBeforeUpsertHook)
	if err = o.doBeforeUpsertHooks(nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpsertHook function to empty object, but got: %#v", o)
	}
	pmsJobTypeFormBeforeUpsertHooks = []PMSJobTypeFormHook{}

	AddPMSJobTypeFormHook(boil.AfterUpsertHook, pmsJobTypeFormAfterUpsertHook)
	if err = o.doAfterUpsertHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpsertHook function to empty object, but got: %#v", o)
	}
	pmsJobTypeFormAfterUpsertHooks = []PMSJobTypeFormHook{}
}
func testPMSJobTypeFormsInsert(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testPMSJobTypeFormsInsertWhitelist(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx, pmsJobTypeFormColumnsWithoutDefault...); err != nil {
		t.Error(err)
	}

	count, err := PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testPMSJobTypeFormToOnePMSFormUsingForm(t *testing.T) {
	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var local PMSJobTypeForm
	var foreign PMSForm

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, pmsJobTypeFormDBTypes, false, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, pmsFormDBTypes, false, pmsFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSForm struct: %s", err)
	}

	if err := foreign.Insert(tx); err != nil {
		t.Fatal(err)
	}

	local.FormID = foreign.ID
	if err := local.Insert(tx); err != nil {
		t.Fatal(err)
	}

	check, err := local.Form(tx).One()
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := PMSJobTypeFormSlice{&local}
	if err = local.L.LoadForm(tx, false, (*[]*PMSJobTypeForm)(&slice)); err != nil {
		t.Fatal(err)
	}
	if local.R.Form == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.Form = nil
	if err = local.L.LoadForm(tx, true, &local); err != nil {
		t.Fatal(err)
	}
	if local.R.Form == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testPMSJobTypeFormToOnePMSJobTypeUsingJobType(t *testing.T) {
	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var local PMSJobTypeForm
	var foreign PMSJobType

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, pmsJobTypeFormDBTypes, false, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, pmsJobTypeDBTypes, false, pmsJobTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobType struct: %s", err)
	}

	if err := foreign.Insert(tx); err != nil {
		t.Fatal(err)
	}

	local.JobTypeID = foreign.ID
	if err := local.Insert(tx); err != nil {
		t.Fatal(err)
	}

	check, err := local.JobType(tx).One()
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := PMSJobTypeFormSlice{&local}
	if err = local.L.LoadJobType(tx, false, (*[]*PMSJobTypeForm)(&slice)); err != nil {
		t.Fatal(err)
	}
	if local.R.JobType == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.JobType = nil
	if err = local.L.LoadJobType(tx, true, &local); err != nil {
		t.Fatal(err)
	}
	if local.R.JobType == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testPMSJobTypeFormToOnePMSStaffUsingCreatedBy(t *testing.T) {
	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var local PMSJobTypeForm
	var foreign PMSStaff

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, pmsStaffDBTypes, false, pmsStaffColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSStaff struct: %s", err)
	}

	local.CreatedByID.Valid = true

	if err := foreign.Insert(tx); err != nil {
		t.Fatal(err)
	}

	local.CreatedByID.Int = foreign.ID
	if err := local.Insert(tx); err != nil {
		t.Fatal(err)
	}

	check, err := local.CreatedBy(tx).One()
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := PMSJobTypeFormSlice{&local}
	if err = local.L.LoadCreatedBy(tx, false, (*[]*PMSJobTypeForm)(&slice)); err != nil {
		t.Fatal(err)
	}
	if local.R.CreatedBy == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.CreatedBy = nil
	if err = local.L.LoadCreatedBy(tx, true, &local); err != nil {
		t.Fatal(err)
	}
	if local.R.CreatedBy == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testPMSJobTypeFormToOnePMSStaffUsingUpdatedBy(t *testing.T) {
	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var local PMSJobTypeForm
	var foreign PMSStaff

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, pmsStaffDBTypes, false, pmsStaffColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSStaff struct: %s", err)
	}

	local.UpdatedByID.Valid = true

	if err := foreign.Insert(tx); err != nil {
		t.Fatal(err)
	}

	local.UpdatedByID.Int = foreign.ID
	if err := local.Insert(tx); err != nil {
		t.Fatal(err)
	}

	check, err := local.UpdatedBy(tx).One()
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := PMSJobTypeFormSlice{&local}
	if err = local.L.LoadUpdatedBy(tx, false, (*[]*PMSJobTypeForm)(&slice)); err != nil {
		t.Fatal(err)
	}
	if local.R.UpdatedBy == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.UpdatedBy = nil
	if err = local.L.LoadUpdatedBy(tx, true, &local); err != nil {
		t.Fatal(err)
	}
	if local.R.UpdatedBy == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testPMSJobTypeFormToOneSetOpPMSFormUsingForm(t *testing.T) {
	var err error

	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSJobTypeForm
	var b, c PMSForm

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsJobTypeFormDBTypes, false, strmangle.SetComplement(pmsJobTypeFormPrimaryKeyColumns, pmsJobTypeFormColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, pmsFormDBTypes, false, strmangle.SetComplement(pmsFormPrimaryKeyColumns, pmsFormColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, pmsFormDBTypes, false, strmangle.SetComplement(pmsFormPrimaryKeyColumns, pmsFormColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(tx); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(tx); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*PMSForm{&b, &c} {
		err = a.SetForm(tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.Form != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.FormPMSJobTypeForms[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.FormID != x.ID {
			t.Error("foreign key was wrong value", a.FormID)
		}

		zero := reflect.Zero(reflect.TypeOf(a.FormID))
		reflect.Indirect(reflect.ValueOf(&a.FormID)).Set(zero)

		if err = a.Reload(tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.FormID != x.ID {
			t.Error("foreign key was wrong value", a.FormID, x.ID)
		}
	}
}
func testPMSJobTypeFormToOneSetOpPMSJobTypeUsingJobType(t *testing.T) {
	var err error

	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSJobTypeForm
	var b, c PMSJobType

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsJobTypeFormDBTypes, false, strmangle.SetComplement(pmsJobTypeFormPrimaryKeyColumns, pmsJobTypeFormColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, pmsJobTypeDBTypes, false, strmangle.SetComplement(pmsJobTypePrimaryKeyColumns, pmsJobTypeColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, pmsJobTypeDBTypes, false, strmangle.SetComplement(pmsJobTypePrimaryKeyColumns, pmsJobTypeColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(tx); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(tx); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*PMSJobType{&b, &c} {
		err = a.SetJobType(tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.JobType != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.JobTypePMSJobTypeForms[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.JobTypeID != x.ID {
			t.Error("foreign key was wrong value", a.JobTypeID)
		}

		zero := reflect.Zero(reflect.TypeOf(a.JobTypeID))
		reflect.Indirect(reflect.ValueOf(&a.JobTypeID)).Set(zero)

		if err = a.Reload(tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.JobTypeID != x.ID {
			t.Error("foreign key was wrong value", a.JobTypeID, x.ID)
		}
	}
}
func testPMSJobTypeFormToOneSetOpPMSStaffUsingCreatedBy(t *testing.T) {
	var err error

	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSJobTypeForm
	var b, c PMSStaff

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsJobTypeFormDBTypes, false, strmangle.SetComplement(pmsJobTypeFormPrimaryKeyColumns, pmsJobTypeFormColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(tx); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(tx); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*PMSStaff{&b, &c} {
		err = a.SetCreatedBy(tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.CreatedBy != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.CreatedByPMSJobTypeForms[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.CreatedByID.Int != x.ID {
			t.Error("foreign key was wrong value", a.CreatedByID.Int)
		}

		zero := reflect.Zero(reflect.TypeOf(a.CreatedByID.Int))
		reflect.Indirect(reflect.ValueOf(&a.CreatedByID.Int)).Set(zero)

		if err = a.Reload(tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.CreatedByID.Int != x.ID {
			t.Error("foreign key was wrong value", a.CreatedByID.Int, x.ID)
		}
	}
}

func testPMSJobTypeFormToOneRemoveOpPMSStaffUsingCreatedBy(t *testing.T) {
	var err error

	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSJobTypeForm
	var b PMSStaff

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsJobTypeFormDBTypes, false, strmangle.SetComplement(pmsJobTypeFormPrimaryKeyColumns, pmsJobTypeFormColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err = a.Insert(tx); err != nil {
		t.Fatal(err)
	}

	if err = a.SetCreatedBy(tx, true, &b); err != nil {
		t.Fatal(err)
	}

	if err = a.RemoveCreatedBy(tx, &b); err != nil {
		t.Error("failed to remove relationship")
	}

	count, err := a.CreatedBy(tx).Count()
	if err != nil {
		t.Error(err)
	}
	if count != 0 {
		t.Error("want no relationships remaining")
	}

	if a.R.CreatedBy != nil {
		t.Error("R struct entry should be nil")
	}

	if a.CreatedByID.Valid {
		t.Error("foreign key value should be nil")
	}

	if len(b.R.CreatedByPMSJobTypeForms) != 0 {
		t.Error("failed to remove a from b's relationships")
	}
}

func testPMSJobTypeFormToOneSetOpPMSStaffUsingUpdatedBy(t *testing.T) {
	var err error

	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSJobTypeForm
	var b, c PMSStaff

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsJobTypeFormDBTypes, false, strmangle.SetComplement(pmsJobTypeFormPrimaryKeyColumns, pmsJobTypeFormColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(tx); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(tx); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*PMSStaff{&b, &c} {
		err = a.SetUpdatedBy(tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.UpdatedBy != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.UpdatedByPMSJobTypeForms[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.UpdatedByID.Int != x.ID {
			t.Error("foreign key was wrong value", a.UpdatedByID.Int)
		}

		zero := reflect.Zero(reflect.TypeOf(a.UpdatedByID.Int))
		reflect.Indirect(reflect.ValueOf(&a.UpdatedByID.Int)).Set(zero)

		if err = a.Reload(tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.UpdatedByID.Int != x.ID {
			t.Error("foreign key was wrong value", a.UpdatedByID.Int, x.ID)
		}
	}
}

func testPMSJobTypeFormToOneRemoveOpPMSStaffUsingUpdatedBy(t *testing.T) {
	var err error

	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSJobTypeForm
	var b PMSStaff

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsJobTypeFormDBTypes, false, strmangle.SetComplement(pmsJobTypeFormPrimaryKeyColumns, pmsJobTypeFormColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err = a.Insert(tx); err != nil {
		t.Fatal(err)
	}

	if err = a.SetUpdatedBy(tx, true, &b); err != nil {
		t.Fatal(err)
	}

	if err = a.RemoveUpdatedBy(tx, &b); err != nil {
		t.Error("failed to remove relationship")
	}

	count, err := a.UpdatedBy(tx).Count()
	if err != nil {
		t.Error(err)
	}
	if count != 0 {
		t.Error("want no relationships remaining")
	}

	if a.R.UpdatedBy != nil {
		t.Error("R struct entry should be nil")
	}

	if a.UpdatedByID.Valid {
		t.Error("foreign key value should be nil")
	}

	if len(b.R.UpdatedByPMSJobTypeForms) != 0 {
		t.Error("failed to remove a from b's relationships")
	}
}

func testPMSJobTypeFormsReload(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	if err = pmsJobTypeForm.Reload(tx); err != nil {
		t.Error(err)
	}
}

func testPMSJobTypeFormsReloadAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	slice := PMSJobTypeFormSlice{pmsJobTypeForm}

	if err = slice.ReloadAll(tx); err != nil {
		t.Error(err)
	}
}
func testPMSJobTypeFormsSelect(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	slice, err := PMSJobTypeForms(tx).All()
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 1 {
		t.Error("want one record, got:", len(slice))
	}
}

var (
	pmsJobTypeFormDBTypes = map[string]string{`CreatedAt`: `timestamp`, `CreatedByID`: `int`, `FormID`: `int`, `ID`: `int`, `JobTypeID`: `int`, `SolfDeleted`: `smallint`, `Status`: `smallint`, `UpdateAt`: `timestamp`, `UpdatedByID`: `int`}
	_                     = bytes.MinRead
)

func testPMSJobTypeFormsUpdate(t *testing.T) {
	t.Parallel()

	if len(pmsJobTypeFormColumns) == len(pmsJobTypeFormPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	if err = pmsJobTypeForm.Update(tx); err != nil {
		t.Error(err)
	}
}

func testPMSJobTypeFormsSliceUpdateAll(t *testing.T) {
	t.Parallel()

	if len(pmsJobTypeFormColumns) == len(pmsJobTypeFormPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	pmsJobTypeForm := &PMSJobTypeForm{}
	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Insert(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, pmsJobTypeForm, pmsJobTypeFormDBTypes, true, pmsJobTypeFormPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	// Remove Primary keys and unique columns from what we plan to update
	var fields []string
	if strmangle.StringSliceMatch(pmsJobTypeFormColumns, pmsJobTypeFormPrimaryKeyColumns) {
		fields = pmsJobTypeFormColumns
	} else {
		fields = strmangle.SetComplement(
			pmsJobTypeFormColumns,
			pmsJobTypeFormPrimaryKeyColumns,
		)
	}

	value := reflect.Indirect(reflect.ValueOf(pmsJobTypeForm))
	updateMap := M{}
	for _, col := range fields {
		updateMap[col] = value.FieldByName(strmangle.TitleCase(col)).Interface()
	}

	slice := PMSJobTypeFormSlice{pmsJobTypeForm}
	if err = slice.UpdateAll(tx, updateMap); err != nil {
		t.Error(err)
	}
}
func testPMSJobTypeFormsUpsert(t *testing.T) {
	t.Parallel()

	if len(pmsJobTypeFormColumns) == len(pmsJobTypeFormPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	// Attempt the INSERT side of an UPSERT
	pmsJobTypeForm := PMSJobTypeForm{}
	if err = randomize.Struct(seed, &pmsJobTypeForm, pmsJobTypeFormDBTypes, true); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsJobTypeForm.Upsert(tx, nil); err != nil {
		t.Errorf("Unable to upsert PMSJobTypeForm: %s", err)
	}

	count, err := PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}

	// Attempt the UPDATE side of an UPSERT
	if err = randomize.Struct(seed, &pmsJobTypeForm, pmsJobTypeFormDBTypes, false, pmsJobTypeFormPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize PMSJobTypeForm struct: %s", err)
	}

	if err = pmsJobTypeForm.Upsert(tx, nil); err != nil {
		t.Errorf("Unable to upsert PMSJobTypeForm: %s", err)
	}

	count, err = PMSJobTypeForms(tx).Count()
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}
}
