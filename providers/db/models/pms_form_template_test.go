// This file is generated by SQLBoiler (https://github.com/vattle/sqlboiler)
// and is meant to be re-generated in place and/or deleted at any time.
// DO NOT EDIT

package models

import (
	"bytes"
	"reflect"
	"testing"

	"github.com/vattle/sqlboiler/boil"
	"github.com/vattle/sqlboiler/randomize"
	"github.com/vattle/sqlboiler/strmangle"
)

func testPMSFormTemplates(t *testing.T) {
	t.Parallel()

	query := PMSFormTemplates(nil)

	if query.Query == nil {
		t.Error("expected a query, got nothing")
	}
}
func testPMSFormTemplatesDelete(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	if err = pmsFormTemplate.Delete(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testPMSFormTemplatesQueryDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	if err = PMSFormTemplates(tx).DeleteAll(); err != nil {
		t.Error(err)
	}

	count, err := PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testPMSFormTemplatesSliceDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	slice := PMSFormTemplateSlice{pmsFormTemplate}

	if err = slice.DeleteAll(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}
func testPMSFormTemplatesExists(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	e, err := PMSFormTemplateExists(tx, pmsFormTemplate.ID)
	if err != nil {
		t.Errorf("Unable to check if PMSFormTemplate exists: %s", err)
	}
	if !e {
		t.Errorf("Expected PMSFormTemplateExistsG to return true, but got false.")
	}
}
func testPMSFormTemplatesFind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	pmsFormTemplateFound, err := FindPMSFormTemplate(tx, pmsFormTemplate.ID)
	if err != nil {
		t.Error(err)
	}

	if pmsFormTemplateFound == nil {
		t.Error("want a record, got nil")
	}
}
func testPMSFormTemplatesBind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	if err = PMSFormTemplates(tx).Bind(pmsFormTemplate); err != nil {
		t.Error(err)
	}
}

func testPMSFormTemplatesOne(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	if x, err := PMSFormTemplates(tx).One(); err != nil {
		t.Error(err)
	} else if x == nil {
		t.Error("expected to get a non nil record")
	}
}

func testPMSFormTemplatesAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplateOne := &PMSFormTemplate{}
	pmsFormTemplateTwo := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplateOne, pmsFormTemplateDBTypes, false, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}
	if err = randomize.Struct(seed, pmsFormTemplateTwo, pmsFormTemplateDBTypes, false, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplateOne.Insert(tx); err != nil {
		t.Error(err)
	}
	if err = pmsFormTemplateTwo.Insert(tx); err != nil {
		t.Error(err)
	}

	slice, err := PMSFormTemplates(tx).All()
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 2 {
		t.Error("want 2 records, got:", len(slice))
	}
}

func testPMSFormTemplatesCount(t *testing.T) {
	t.Parallel()

	var err error
	seed := randomize.NewSeed()
	pmsFormTemplateOne := &PMSFormTemplate{}
	pmsFormTemplateTwo := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplateOne, pmsFormTemplateDBTypes, false, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}
	if err = randomize.Struct(seed, pmsFormTemplateTwo, pmsFormTemplateDBTypes, false, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplateOne.Insert(tx); err != nil {
		t.Error(err)
	}
	if err = pmsFormTemplateTwo.Insert(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Error("want 2 records, got:", count)
	}
}
func pmsFormTemplateBeforeInsertHook(e boil.Executor, o *PMSFormTemplate) error {
	*o = PMSFormTemplate{}
	return nil
}

func pmsFormTemplateAfterInsertHook(e boil.Executor, o *PMSFormTemplate) error {
	*o = PMSFormTemplate{}
	return nil
}

func pmsFormTemplateAfterSelectHook(e boil.Executor, o *PMSFormTemplate) error {
	*o = PMSFormTemplate{}
	return nil
}

func pmsFormTemplateBeforeUpdateHook(e boil.Executor, o *PMSFormTemplate) error {
	*o = PMSFormTemplate{}
	return nil
}

func pmsFormTemplateAfterUpdateHook(e boil.Executor, o *PMSFormTemplate) error {
	*o = PMSFormTemplate{}
	return nil
}

func pmsFormTemplateBeforeDeleteHook(e boil.Executor, o *PMSFormTemplate) error {
	*o = PMSFormTemplate{}
	return nil
}

func pmsFormTemplateAfterDeleteHook(e boil.Executor, o *PMSFormTemplate) error {
	*o = PMSFormTemplate{}
	return nil
}

func pmsFormTemplateBeforeUpsertHook(e boil.Executor, o *PMSFormTemplate) error {
	*o = PMSFormTemplate{}
	return nil
}

func pmsFormTemplateAfterUpsertHook(e boil.Executor, o *PMSFormTemplate) error {
	*o = PMSFormTemplate{}
	return nil
}

func testPMSFormTemplatesHooks(t *testing.T) {
	t.Parallel()

	var err error

	empty := &PMSFormTemplate{}
	o := &PMSFormTemplate{}

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, o, pmsFormTemplateDBTypes, false); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate object: %s", err)
	}

	AddPMSFormTemplateHook(boil.BeforeInsertHook, pmsFormTemplateBeforeInsertHook)
	if err = o.doBeforeInsertHooks(nil); err != nil {
		t.Errorf("Unable to execute doBeforeInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeInsertHook function to empty object, but got: %#v", o)
	}
	pmsFormTemplateBeforeInsertHooks = []PMSFormTemplateHook{}

	AddPMSFormTemplateHook(boil.AfterInsertHook, pmsFormTemplateAfterInsertHook)
	if err = o.doAfterInsertHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterInsertHook function to empty object, but got: %#v", o)
	}
	pmsFormTemplateAfterInsertHooks = []PMSFormTemplateHook{}

	AddPMSFormTemplateHook(boil.AfterSelectHook, pmsFormTemplateAfterSelectHook)
	if err = o.doAfterSelectHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterSelectHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterSelectHook function to empty object, but got: %#v", o)
	}
	pmsFormTemplateAfterSelectHooks = []PMSFormTemplateHook{}

	AddPMSFormTemplateHook(boil.BeforeUpdateHook, pmsFormTemplateBeforeUpdateHook)
	if err = o.doBeforeUpdateHooks(nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpdateHook function to empty object, but got: %#v", o)
	}
	pmsFormTemplateBeforeUpdateHooks = []PMSFormTemplateHook{}

	AddPMSFormTemplateHook(boil.AfterUpdateHook, pmsFormTemplateAfterUpdateHook)
	if err = o.doAfterUpdateHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpdateHook function to empty object, but got: %#v", o)
	}
	pmsFormTemplateAfterUpdateHooks = []PMSFormTemplateHook{}

	AddPMSFormTemplateHook(boil.BeforeDeleteHook, pmsFormTemplateBeforeDeleteHook)
	if err = o.doBeforeDeleteHooks(nil); err != nil {
		t.Errorf("Unable to execute doBeforeDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeDeleteHook function to empty object, but got: %#v", o)
	}
	pmsFormTemplateBeforeDeleteHooks = []PMSFormTemplateHook{}

	AddPMSFormTemplateHook(boil.AfterDeleteHook, pmsFormTemplateAfterDeleteHook)
	if err = o.doAfterDeleteHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterDeleteHook function to empty object, but got: %#v", o)
	}
	pmsFormTemplateAfterDeleteHooks = []PMSFormTemplateHook{}

	AddPMSFormTemplateHook(boil.BeforeUpsertHook, pmsFormTemplateBeforeUpsertHook)
	if err = o.doBeforeUpsertHooks(nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpsertHook function to empty object, but got: %#v", o)
	}
	pmsFormTemplateBeforeUpsertHooks = []PMSFormTemplateHook{}

	AddPMSFormTemplateHook(boil.AfterUpsertHook, pmsFormTemplateAfterUpsertHook)
	if err = o.doAfterUpsertHooks(nil); err != nil {
		t.Errorf("Unable to execute doAfterUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpsertHook function to empty object, but got: %#v", o)
	}
	pmsFormTemplateAfterUpsertHooks = []PMSFormTemplateHook{}
}
func testPMSFormTemplatesInsert(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testPMSFormTemplatesInsertWhitelist(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx, pmsFormTemplateColumnsWithoutDefault...); err != nil {
		t.Error(err)
	}

	count, err := PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testPMSFormTemplateToManyTemplatePMSForms(t *testing.T) {
	var err error
	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSFormTemplate
	var b, c PMSForm

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	if err := a.Insert(tx); err != nil {
		t.Fatal(err)
	}

	randomize.Struct(seed, &b, pmsFormDBTypes, false, pmsFormColumnsWithDefault...)
	randomize.Struct(seed, &c, pmsFormDBTypes, false, pmsFormColumnsWithDefault...)

	b.TemplateID = a.ID
	c.TemplateID = a.ID
	if err = b.Insert(tx); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(tx); err != nil {
		t.Fatal(err)
	}

	pmsForm, err := a.TemplatePMSForms(tx).All()
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range pmsForm {
		if v.TemplateID == b.TemplateID {
			bFound = true
		}
		if v.TemplateID == c.TemplateID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := PMSFormTemplateSlice{&a}
	if err = a.L.LoadTemplatePMSForms(tx, false, (*[]*PMSFormTemplate)(&slice)); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.TemplatePMSForms); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.TemplatePMSForms = nil
	if err = a.L.LoadTemplatePMSForms(tx, true, &a); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.TemplatePMSForms); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", pmsForm)
	}
}

func testPMSFormTemplateToManyAddOpTemplatePMSForms(t *testing.T) {
	var err error

	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSFormTemplate
	var b, c, d, e PMSForm

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsFormTemplateDBTypes, false, strmangle.SetComplement(pmsFormTemplatePrimaryKeyColumns, pmsFormTemplateColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*PMSForm{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, pmsFormDBTypes, false, strmangle.SetComplement(pmsFormPrimaryKeyColumns, pmsFormColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(tx); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(tx); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(tx); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*PMSForm{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddTemplatePMSForms(tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if a.ID != first.TemplateID {
			t.Error("foreign key was wrong value", a.ID, first.TemplateID)
		}
		if a.ID != second.TemplateID {
			t.Error("foreign key was wrong value", a.ID, second.TemplateID)
		}

		if first.R.Template != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.Template != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.TemplatePMSForms[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.TemplatePMSForms[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.TemplatePMSForms(tx).Count()
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}
func testPMSFormTemplateToOnePMSStaffUsingCreatedBy(t *testing.T) {
	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var local PMSFormTemplate
	var foreign PMSStaff

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, pmsFormTemplateDBTypes, false, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, pmsStaffDBTypes, false, pmsStaffColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSStaff struct: %s", err)
	}

	if err := foreign.Insert(tx); err != nil {
		t.Fatal(err)
	}

	local.CreatedByID = foreign.ID
	if err := local.Insert(tx); err != nil {
		t.Fatal(err)
	}

	check, err := local.CreatedBy(tx).One()
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := PMSFormTemplateSlice{&local}
	if err = local.L.LoadCreatedBy(tx, false, (*[]*PMSFormTemplate)(&slice)); err != nil {
		t.Fatal(err)
	}
	if local.R.CreatedBy == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.CreatedBy = nil
	if err = local.L.LoadCreatedBy(tx, true, &local); err != nil {
		t.Fatal(err)
	}
	if local.R.CreatedBy == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testPMSFormTemplateToOnePMSStaffUsingUpdatedBy(t *testing.T) {
	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var local PMSFormTemplate
	var foreign PMSStaff

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, pmsFormTemplateDBTypes, false, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, pmsStaffDBTypes, false, pmsStaffColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSStaff struct: %s", err)
	}

	if err := foreign.Insert(tx); err != nil {
		t.Fatal(err)
	}

	local.UpdatedByID = foreign.ID
	if err := local.Insert(tx); err != nil {
		t.Fatal(err)
	}

	check, err := local.UpdatedBy(tx).One()
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := PMSFormTemplateSlice{&local}
	if err = local.L.LoadUpdatedBy(tx, false, (*[]*PMSFormTemplate)(&slice)); err != nil {
		t.Fatal(err)
	}
	if local.R.UpdatedBy == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.UpdatedBy = nil
	if err = local.L.LoadUpdatedBy(tx, true, &local); err != nil {
		t.Fatal(err)
	}
	if local.R.UpdatedBy == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testPMSFormTemplateToOneSetOpPMSStaffUsingCreatedBy(t *testing.T) {
	var err error

	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSFormTemplate
	var b, c PMSStaff

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsFormTemplateDBTypes, false, strmangle.SetComplement(pmsFormTemplatePrimaryKeyColumns, pmsFormTemplateColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(tx); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(tx); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*PMSStaff{&b, &c} {
		err = a.SetCreatedBy(tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.CreatedBy != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.CreatedByPMSFormTemplates[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.CreatedByID != x.ID {
			t.Error("foreign key was wrong value", a.CreatedByID)
		}

		zero := reflect.Zero(reflect.TypeOf(a.CreatedByID))
		reflect.Indirect(reflect.ValueOf(&a.CreatedByID)).Set(zero)

		if err = a.Reload(tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.CreatedByID != x.ID {
			t.Error("foreign key was wrong value", a.CreatedByID, x.ID)
		}
	}
}
func testPMSFormTemplateToOneSetOpPMSStaffUsingUpdatedBy(t *testing.T) {
	var err error

	tx := MustTx(boil.Begin())
	defer tx.Rollback()

	var a PMSFormTemplate
	var b, c PMSStaff

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, pmsFormTemplateDBTypes, false, strmangle.SetComplement(pmsFormTemplatePrimaryKeyColumns, pmsFormTemplateColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, pmsStaffDBTypes, false, strmangle.SetComplement(pmsStaffPrimaryKeyColumns, pmsStaffColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(tx); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(tx); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*PMSStaff{&b, &c} {
		err = a.SetUpdatedBy(tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.UpdatedBy != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.UpdatedByPMSFormTemplates[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.UpdatedByID != x.ID {
			t.Error("foreign key was wrong value", a.UpdatedByID)
		}

		zero := reflect.Zero(reflect.TypeOf(a.UpdatedByID))
		reflect.Indirect(reflect.ValueOf(&a.UpdatedByID)).Set(zero)

		if err = a.Reload(tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.UpdatedByID != x.ID {
			t.Error("foreign key was wrong value", a.UpdatedByID, x.ID)
		}
	}
}
func testPMSFormTemplatesReload(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	if err = pmsFormTemplate.Reload(tx); err != nil {
		t.Error(err)
	}
}

func testPMSFormTemplatesReloadAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	slice := PMSFormTemplateSlice{pmsFormTemplate}

	if err = slice.ReloadAll(tx); err != nil {
		t.Error(err)
	}
}
func testPMSFormTemplatesSelect(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	slice, err := PMSFormTemplates(tx).All()
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 1 {
		t.Error("want one record, got:", len(slice))
	}
}

var (
	pmsFormTemplateDBTypes = map[string]string{`CreateAt`: `datetime`, `CreatedByID`: `int`, `Description`: `varchar`, `ID`: `int`, `Name`: `varchar`, `UpdatedAt`: `datetime`, `UpdatedByID`: `int`}
	_                      = bytes.MinRead
)

func testPMSFormTemplatesUpdate(t *testing.T) {
	t.Parallel()

	if len(pmsFormTemplateColumns) == len(pmsFormTemplatePrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	if err = pmsFormTemplate.Update(tx); err != nil {
		t.Error(err)
	}
}

func testPMSFormTemplatesSliceUpdateAll(t *testing.T) {
	t.Parallel()

	if len(pmsFormTemplateColumns) == len(pmsFormTemplatePrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	pmsFormTemplate := &PMSFormTemplate{}
	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplateColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Insert(tx); err != nil {
		t.Error(err)
	}

	count, err := PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, pmsFormTemplate, pmsFormTemplateDBTypes, true, pmsFormTemplatePrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	// Remove Primary keys and unique columns from what we plan to update
	var fields []string
	if strmangle.StringSliceMatch(pmsFormTemplateColumns, pmsFormTemplatePrimaryKeyColumns) {
		fields = pmsFormTemplateColumns
	} else {
		fields = strmangle.SetComplement(
			pmsFormTemplateColumns,
			pmsFormTemplatePrimaryKeyColumns,
		)
	}

	value := reflect.Indirect(reflect.ValueOf(pmsFormTemplate))
	updateMap := M{}
	for _, col := range fields {
		updateMap[col] = value.FieldByName(strmangle.TitleCase(col)).Interface()
	}

	slice := PMSFormTemplateSlice{pmsFormTemplate}
	if err = slice.UpdateAll(tx, updateMap); err != nil {
		t.Error(err)
	}
}
func testPMSFormTemplatesUpsert(t *testing.T) {
	t.Parallel()

	if len(pmsFormTemplateColumns) == len(pmsFormTemplatePrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	// Attempt the INSERT side of an UPSERT
	pmsFormTemplate := PMSFormTemplate{}
	if err = randomize.Struct(seed, &pmsFormTemplate, pmsFormTemplateDBTypes, true); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	tx := MustTx(boil.Begin())
	defer tx.Rollback()
	if err = pmsFormTemplate.Upsert(tx, nil); err != nil {
		t.Errorf("Unable to upsert PMSFormTemplate: %s", err)
	}

	count, err := PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}

	// Attempt the UPDATE side of an UPSERT
	if err = randomize.Struct(seed, &pmsFormTemplate, pmsFormTemplateDBTypes, false, pmsFormTemplatePrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize PMSFormTemplate struct: %s", err)
	}

	if err = pmsFormTemplate.Upsert(tx, nil); err != nil {
		t.Errorf("Unable to upsert PMSFormTemplate: %s", err)
	}

	count, err = PMSFormTemplates(tx).Count()
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}
}
