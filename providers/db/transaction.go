package db

import (
	"database/sql"
	"github.com/pkg/errors"
)

type transactionManager struct {
	db *sql.DB
	Transaction *sql.Tx
}

func NewTransaction(db *sql.DB) transactionManager {
	return transactionManager{db:db}
}

func (this transactionManager) Begin(){
	tx, err := this.db.Begin()
	if err != nil{
		panic(err)
	}
	this.Transaction = tx
}

func (this transactionManager) Commit() error{
	if this.Transaction == nil{
		// TODO use a logger to log this error. Centralize error logging
		return errors.New("Cannot commit a transaction that have not started")
	}
	err := this.Transaction.Commit()
	return err
}

func (this transactionManager) Rollback() error{
	if this.Transaction == nil{
		// TODO use a logger to log this error. Centralize error logging
		return errors.New("Cannot commit a transaction that have not started")
	}
	err := this.Transaction.Rollback()
	return err
}
