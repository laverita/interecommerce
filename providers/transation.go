package providers

type ITransactionManager interface {
	Begin()
	Commit() error
	Rollback() error
}
