package crud

import (
	"bitbucket.org/superfluxteam/cruder"
	"bitbucket.org/superfluxteam/cruder/navigation"
	"bitbucket.org/laverita/interecommerce/domain"
	"net/http"
	"bitbucket.org/superfluxteam/cruder/roles"
	"bitbucket.org/laverita/interecommerce/db"
	"bitbucket.org/laverita/interecommerce/services"
)

func InitCruderResources(cr *cruder.Cruder) *cruder.Cruder {

	cr.AddResource(domain.Bank{},
		&cruder.Config{NewAttributes: []string{"-BankAccounts"}},
		&cruder.Config{EditAttributes: []string{"-BankAccounts"}},
		&cruder.Config{IndexAttributes: []string{"Name"}})
	cr.AddResource(domain.BankAccount{}, )
	cr.AddResource(domain.Status{})
	cr.AddResource(navigation.Menu{})

	cr.AddResource(domain.City{})
	cr.AddResource(domain.State{}, &cruder.Config{
		IndexAttributes: []string{"-Cities"},
		NewAttributes: []string{"-Cities"},
		EditAttributes: []string{"-Cities"},
	})


	//acl
	cr.AddResource(domain.Application{})
	cr.AddResource(domain.Permission{})
	cr.AddResource(domain.Role{})
	cr.AddResource(domain.RolePermission{})
	cr.AddResource(domain.Department{})
	cr.AddResource(domain.Staff{}, &cruder.Config{NewAttributes: []string{"-Roles"},
		IndexAttributes:[]string{"-Roles"}})

	cr.AddResource(domain.Manufacturer{})
	cr.AddResource(domain.Brand{})
	cr.AddResource(domain.Product{})
	cr.AddResource(domain.Stock{})

	cr.AddResource(&domain.Market{})
	cr.AddResource(&domain.Order{})
	cr.AddResource(&domain.OrderItem{})
	cr.AddResource(&domain.OrderHistory{})
	cr.AddResource(domain.Sale{})
	cr.AddResource(domain.Payment{})

	cr.AddResource(domain.Income{})
	cr.AddResource(domain.Expenditure{})


	initPermissions(cr)

	return cr
}



func initPermissions(c *cruder.Cruder) {
	roles.InitPermissionMap = func() error {
		_db := db.GetDb()
		var _roles []domain.Role
		err := _db.Preload("Permissions").Find(&_roles).Error
		if err != nil {
			return err
		}
		aclService := services.AclService{Db: _db}

		for _, role := range _roles {
			// register the role
			c.Permission.Role.Register(role.Name, func(req *http.Request, user interface{}) bool {
				return aclService.StaffIsInRole(user.(*domain.Staff).ID, role.ID)
			})

			// grant all the permission
			for _, permission := range role.Permissions {
				c.Permission.Allow(roles.PermissionMode(permission.Name), role.Name)
			}
		}
		return nil
	}

	var _rolePermissionMap map[roles.PermissionMode]uint
	roles.PermissionModeToIdMap = func(reload bool) (rolePermissionMap map[roles.PermissionMode]uint) {
		if _rolePermissionMap != nil && !reload {
			return _rolePermissionMap
		}

		db := db.GetDb()
		var permissions []domain.Permission
		db.Find(&permissions)
		rolePermissionMap = make(map[roles.PermissionMode]uint)
		for _, permission := range permissions {
			rolePermissionMap[roles.PermissionMode(permission.Name)] = permission.ID
		}
		_rolePermissionMap = rolePermissionMap
		return
	}

	roles.PermissionCreator = func(mode roles.PermissionMode) uint {
		var permission domain.Permission
		permission.Name = string(mode)
		db.GetDb().Save(&permission)
		return permission.ID
	}


}