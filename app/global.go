package app

import (
	"github.com/ccding/go-logging/logging"
	"bitbucket.org/superfluxteam/cruder"
)

var (
	Logger *logging.Logger
	Cruder *cruder.Cruder
)

const (
	MOBILE_CLIENT = "mobile_client"
	WEB_CLIENT = "web_client"

	HEADER_KEY_LATITUDE = "latitude"
	HEADER_KEY_LONGITUDE = "longitude"
	HEADER_KEY_STAFF_ID = "staff_id"
)
