package acl

import (
	"net/http"
	"errors"
	"strconv"
	"bitbucket.org/laverita/interecommerce/adapter/web"
	"bitbucket.org/laverita/interecommerce/services"
	"bitbucket.org/laverita/interecommerce/adapter/web/resources"
	"bitbucket.org/laverita/interecommerce/db"
	"bitbucket.org/laverita/interecommerce/app"
	"github.com/labstack/echo"
	"bitbucket.org/laverita/interecommerce/domain"
)

//TODO read token from every request and add it to context
type loginModel struct {
	Username	string		`json:"username"`
	Password 	string		`json:"password"`
}

type loginResponse struct {
	domain.Staff
	Permissions []uint	`json:"permissions"`
}

func AuthLogin(c echo.Context) error {
	var model loginModel
	var token string
	// Decode the incoming login json
	err := c.Bind(&model)
	if err != nil {
		app.Logger.Error(err)
		return web.InvalidRequestResponse(c)
	}

	db := db.GetDb()
	userService := services.UserService{Db: db}
	// Authenticate the login staff
	if staff, err := userService.Login(model.Username, model.Password); err != nil {
		return web.SendError(c, err, "Invalid credentials")
	} else {
		// if login is successful
		service := services.AclService{Db:db}

		permissions := service.GetPermissionsForStaff(staff.ID)
		// Generate json web token
		tokenData := web.TokenData{UserId: staff.ID, Permissions: permissions}
		token, err = web.GenerateJWT(tokenData)
		if err != nil {
			app.Logger.Error(err)
			return web.SendError(c, err, "Error while generating access token")
		}
		staff.Password = ""
		if err != nil{
			return web.SendError(c, err, "Staff record not found for staff")
		}

		c.Set("token", token)

		response := loginResponse{staff, permissions}

		return web.SendSuccess(c, http.StatusOK, response)
	}
}

func ChangePassword(c echo.Context) error {
	var model resources.ChangePasswordModel
	// Decode the incoming json
	err := c.Bind(&model)
	if err != nil {
		return web.SendError(c, err, "Invalid request data")
	}

	db := db.GetDb()
	//defer db.Close()
	userService := services.UserService{Db:db}
	// check to see that the newpassword matches its confirmation
	if model.NewPassword != model.ConfirmPassword{
		return web.SendError(c, errors.New("Wrong password confirmation"), "Wrong password confirmation")
	}
	currentUser := c.Request().Context().Value("UserInfo").(map[string]interface{})
	err = userService.ChangePassword(uint(currentUser["StaffId"].(float32)), model.OldPassword, model.NewPassword)
	if err != nil{
		return web.SendError(c, err, err.Error())
	}
	return web.SendSuccess(c, http.StatusOK, true)
}

// ChangePasswordForId changes the password of a user specified by the id
// Handler /auth/:id/changepassword [POST]
func ChangePasswordForId(c echo.Context) error {
	//get id from in coming request
	id, err := strconv.ParseInt(c.Param("id"), 10, 32)
	if err != nil{
		return web.SendError(c, err, "Invalid Staff ID")
	}

	var model resources.ChangePasswordModel
	// Decode the incoming json
	err = c.Bind(&model)
	if err != nil {
		return web.SendError(c, err, "Invalid request data")
	}

	db := db.GetDb()
	//defer db.Close()
	userService := services.UserService{Db:db}

	err = userService.ChangePasswordForId(uint(id), model.NewPassword)
	if err != nil{
		return web.SendError(c, err, err.Error())
	}
	return web.SendSuccess(c, http.StatusOK, true)

}