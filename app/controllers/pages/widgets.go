package pages

import (
	"bitbucket.org/superfluxteam/cruder"
	"bitbucket.org/laverita/interecommerce/domain"
)


func totalIncomeWidget(cr *cruder.Cruder) *cruder.Widget {
	totalIncomeWidget := cr.Widget("total_income", "Total Income", "" , cruder.WIDGET_TYPE_TILE)
	totalIncomeWidget.Columns = []cruder.Column{{Label:"Total Income", Name:"total_income"}}
	totalIncomeWidget.ArgFields = []string{"start_date", "end_date"}
	totalIncomeWidget.FetchData = func(filter map[string]interface{}, context *cruder.Context) (data []map[string]interface{}, err error) {
		var result struct {
			TotalIncome float64	`json:"total_income"`
		}
		err = context.DB.Model(&domain.Income{}).Select("SUM(amount) as total_income").Scan(&result).Error

		if err != nil {
			return []map[string]interface{}{}, err
		}
		return []map[string]interface{}{{"total_income":result.TotalIncome}}, nil
	}
	return totalIncomeWidget
}

func totalExpenditureWidget(cr *cruder.Cruder) *cruder.Widget {
	totalIncomeWidget := cr.Widget("total_expenditure", "Total Expenditure", "" , cruder.WIDGET_TYPE_TILE)
	totalIncomeWidget.Columns = []cruder.Column{{Label:"Total Expenditure", Name:"total"}}
	totalIncomeWidget.ArgFields = []string{"start_date", "end_date"}
	totalIncomeWidget.FetchData = func(filter map[string]interface{}, context *cruder.Context) (data []map[string]interface{}, err error) {
		var result struct {
			Total float64	`json:"total"`
		}
		err = context.DB.Model(&domain.Expenditure{}).Select("SUM(amount) as total").Scan(&result).Error

		if err != nil {
			return []map[string]interface{}{}, err
		}
		return []map[string]interface{}{{"total":result.Total}}, nil
	}
	return totalIncomeWidget
}

func netBalanceWidget(cr *cruder.Cruder) *cruder.Widget {
	totalIncomeWidget := cr.Widget("net_balance", "Net Balance", "" , cruder.WIDGET_TYPE_TILE)
	totalIncomeWidget.Columns = []cruder.Column{{Label:"Net Balance", Name:"total"}}
	totalIncomeWidget.ArgFields = []string{"start_date", "end_date"}
	totalIncomeWidget.FetchData = func(filter map[string]interface{}, context *cruder.Context) (data []map[string]interface{}, err error) {
		var result struct {
			Total float64	`json:"total"`
		}
		err = context.DB.Model(&domain.Income{}).Select("SUM(amount) as total").Scan(&result).Error

		if err != nil {
			return []map[string]interface{}{}, err
		}
		income := result.Total

		err = context.DB.Model(&domain.Expenditure{}).Select("SUM(amount) as total").Scan(&result).Error

		if err != nil {
			return []map[string]interface{}{}, err
		}
		expenditure := result.Total

		return []map[string]interface{}{{"total":income - expenditure}}, nil
	}
	return totalIncomeWidget
}
