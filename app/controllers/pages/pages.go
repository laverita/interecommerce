package pages

import (
	"bitbucket.org/superfluxteam/cruder"
	"bitbucket.org/superfluxteam/cruder/roles"
)

func InitPages(cr *cruder.Cruder) *cruder.Cruder {
	dashboard := cr.Page("dashboard", "Dashboard", "Application Dashboard")
	dashboard.PermissionMode = roles.PermissionMode("dashboard")
	dashboard.Widget(totalIncomeWidget(cr), 0)
	dashboard.Widget(totalExpenditureWidget(cr), 0)
	dashboard.Widget(netBalanceWidget(cr), 0)

	finance := cr.Page("finance", "Financial Report", "")
	finance.Widget(totalIncomeWidget(cr), 0)
	finance.Widget(totalExpenditureWidget(cr), 0)
	finance.Widget(netBalanceWidget(cr), 0)

	return cr
}
