package routers

import (
	"github.com/labstack/echo"
)

func InitRoutes(r *echo.Echo)  {
	g := r.Group("/api/v1")
	SetAuthRoute(g)
	SetSecurityRoute(g)
}