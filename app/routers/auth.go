package routers

import (
	"bitbucket.org/laverita/interecommerce/app/controllers/acl"
	"github.com/labstack/echo"
)

func SetAuthRoute(router *echo.Group) {
	router.POST("/auth/login", acl.AuthLogin)


	//applicationRoute.HandleFunc("/auth/register", controllers.AuthRegister).Methods("POST")
	router.POST("/auth/changepassword", acl.ChangePassword)
	router.POST("/auth/:id/changepassword", acl.ChangePasswordForId)

}

func SetSecurityRoute(r *echo.Group)  {
	r.POST("/auth/login", acl.AuthLogin)
	r.POST("/auth/changepassword", acl.ChangePassword)
}