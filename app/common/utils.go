package common

import (
	"encoding/json"
	"log"
	"os"
	"fmt"
	"reflect"
	"errors"
)

type configuration struct {
	DbHost, DbUserName, DbPassword, Database, Server, Port string
	TokenLifeTime, DefaultPageSize int
	ApiVersion string
}

//Initialize AppConfig
var AppConfig configuration

func initConfig() {
	loadConfig()
}

func loadConfig() {
	file, err := os.Open("app/common/config.json")
	defer file.Close()
	if err != nil {
		log.Fatalf("[loadConfig]: %s\n", err)
	}
	decoder := json.NewDecoder(file)
	AppConfig = configuration{}
	err = decoder.Decode(&AppConfig)
	if err != nil {
		log.Fatalf("[loadConfig]: %s\n", err)
	}
}


func Log(key string, data interface{})  {
	fmt.Printf("%s: %v\n", key, data)
}


func SetField(obj interface{}, name string, value interface{}) error {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)

	if !structFieldValue.IsValid() {
		return fmt.Errorf("No such field: %s in obj", name)
	}

	if !structFieldValue.CanSet() {
		return fmt.Errorf("Cannot set %s field value", name)
	}

	structFieldType := structFieldValue.Type()
	val := reflect.ValueOf(value)
	if structFieldType != val.Type() {
		invalidTypeError := errors.New("Provided value type didn't match obj field type")
		return invalidTypeError
	}

	structFieldValue.Set(val)
	return nil
}



