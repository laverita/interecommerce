package common

import (
	"crypto/rsa"
	"github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"log"
	"time"
)

// using asymmetric crypto/RSA keys
const (
	// openssl genrsa -out app.rsa
	privatePath = "app/common/keys/app.rsa"
	// openssl rsa -in app.rsa -pubout > app.rsa.pub
	publicPath = "app/common/keys/app.rsa.pub"
)

// private key for signing and public key for verification
var (
	//verifyKey, signKey []byte

	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
)

type TokenData struct {
	UserId int64
	Permissions []int64
}


// Read key files before starting http handlers
func initKeys() {
	var err error

	signBytes, err := ioutil.ReadFile(privatePath)
	if err != nil {
		log.Fatalf("[initKeys]: %s\n", err)
	}

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		log.Fatalf("[initKeys]: %s\n", err)
	}

	verifyBytes, err := ioutil.ReadFile(publicPath)
	if err != nil {
		log.Fatalf("[initKeys]: %s\n", err)
	}

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		log.Fatalf("[initKeys]: %s\n", err)
	}
}

// Generate JWT token
func GenerateJWT(tokenData TokenData) (string, error) {

	claims := make(jwt.MapClaims)

	//set claim for JWT token
	claims["iss"] = "admin"
	claims["UserInfo"] = tokenData

	// set the expire time for JWT token
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(AppConfig.TokenLifeTime)).Unix()
	claims["iat"] = time.Now().Unix()

	//t.Claims = claims

	// create a signer for rsa 256
	t := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	tokenString, err := t.SignedString(signKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}