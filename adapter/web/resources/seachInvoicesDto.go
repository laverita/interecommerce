package resources

import (
	"time"
)

type SearchInvoicesDto struct {

	InvoiceID 				uint			`json:"invoice_id"`
	InvoiceAmount			float64			`json:"invoice_amount"`
	InvoiceCreatedAt		time.Time		`json:"invoice_created_at"`
	InvoiceDiscount			float64			`json:"invoice_discount"`
	InvoiceNumber			string			`json:"invoice_number"`
	CustomerID				uint			`json:"customer_id"`
	CustomerName			string			`json:"customer_name"`
	CustomerBusinessName	string			`json:"customer_business_name"`
	CustomerPhoneNumber		string			`json:"customer_phone_number"`
	PaymentTimes			int				`json:"payment_times"`
}

