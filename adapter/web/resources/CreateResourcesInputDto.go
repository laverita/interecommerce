package resources


type CreateResourcesInputDto struct {
	Keys 	[]string	`json:"resources"`
	ApplicationId 	uint	`json:"application_id"`
}

type CreateResourcesOutputDto struct {
	Key 	string		`json:"key"`
	Message string		`json:"message"`
	Success bool		`json:"success"`
}
