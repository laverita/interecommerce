package resources

type CreateStockRequestModel struct {
	Products []product    `json:"products"`
	StaffId uint     	  `json:"staff_id"`
}
type product struct {
	ProductId uint 		`json:"product_id"`
	Quantity  uint   		    `json:"quantity"`
}