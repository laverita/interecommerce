package resources

import "net/http"

type ActivateDeviceInput struct {
	Ime 		string		`json:"ime"`
	DeviceName 	string		`json:"device_name"`
	StaffNo 	string		`json:"staff_no"`
	ActivationCode 	string		`json:"activation_code"`
}

func (a ActivateDeviceInput) Bind(r *http.Request) error {
	return nil
}