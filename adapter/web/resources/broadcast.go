package resources

import (
	"time"
)

type BroadcastMessageDto struct {
	MessageIdOnline	uint		`json:"message_id_online"`
	CreatedAt 	time.Time	`json:"created_at"`
	UpdatedAt 	time.Time	`json:"updated_at"`
	DeletedAt 	time.Time	`json:"deleted_at"`
	Title 		string		`json:"title"`
	Message 	string		`json:"message"`
	StaffId		uint            `json:"staff_id"`
	Status		uint		`json:"status"`


}
