package resources

type ChangePasswordModel struct {
	OldPassword, NewPassword, ConfirmPassword string
}
