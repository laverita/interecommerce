package resources

import (
	"time"
)

type InvoicesPaymentDto struct {

	InvoiceAmount			float64			`json:"invoice_amount"`
	PaymentCreatedAt		time.Time		`json:"payment_created_at"`
	InvoiceNumber			string			`json:"invoice_number"`
	CustomerName			string			`json:"customer_name"`
	CustomerBusinessName	string			`json:"customer_business_name"`
	CustomerPhoneNumber		string			`json:"customer_phone_number"`
	PaymentTimes			int				`json:"payment_times"`
	PaymentType				string			`json:"payment_type"`
	PaymentInvoiceAmount	float64			`json:"payment_invoice_amount"`
}
