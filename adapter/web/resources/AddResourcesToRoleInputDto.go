package resources

type AddResourcesToRoleInputDto struct {
	ResourceIds		[]uint		`json:"resource_ids"`
	RoleId			uint		`json:"role_id"`
}
