package resources

import (
	"time"
)

type RoutePlanDto struct {
	ID				uint		`json:"id"`
	Day				string		`json:"day"`
	RouteID			uint		`json:"route_id"`
	CreatedAt 		time.Time	`json:"created_at"`
	UpdatedAt 		time.Time	`json:"updated_at"`
	DeletedAt 		time.Time	`json:"deleted_at"`
	CustomerID		uint		`json:"customer_id"`
	Name 			string		`json:"name"`
	CustomerName	string 		`json:"customer_name"`
	Email	 		string		`json:"email"`
	PhoneNumber		string		`json:"phone_number"`
	Address			string		`json:"address"`
	JoinDate		string		`json:"join_date"`
	SalesRepID		uint     	`json:"staff_id"`

}
