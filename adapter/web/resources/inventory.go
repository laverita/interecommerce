package resources

import (
	"time"
)

type StockSummary struct {
	ProductId 	uint	`json:"id"`
	ProductName	string	`json:"product_name"`
	Quantity	uint	`json:"quantity"`
}

type StockInfo struct {
	StockId		uint		`json:"id"`
	ProductId	uint		`json:"product_id"`
	ProductName	string		`json:"product_name"`
	Quantity	uint		`json:"quantity"`

	BatchNumber	string		`json:"batch_number"`
	BestBeforeDate	time.Time	`json:"best_before_date"`
}

type StockDeductionOutput struct {
	//Stocks holds a map of stock id => quantity in a deduction
	Stocks map[uint]uint
}