package resources

type ResponseResource struct {
	StatusCode int `json:"status"`
	Success bool `json:"success"`
	Message string `json:"message"`
	Data interface{} `json:"data"`
	Token string `json:"token"`
}

type PagedResultRequest struct {
	Page  int `json:"count" form:"count" query:"count"`
	PageSize  int `json:"page_size" form:"page_size" query:"page_size"`
}

func (p PagedResultRequest) GetOffset() int {
	if p.Page == 0{
		return 0
	}
	if p.PageSize == 0{
		p.PageSize = 20
	}
	return (p.Page - 1) * p.PageSize
}
