package resources

import (
	"time"
)

type CustomerRepDto struct {
	SalesRepId   uint        `json:"sales_rep_id"`
	CustomerId   uint        `json:"customer_id"`
	CreatedAt    time.Time   `json:"created_at"`
	UpdatedAt    time.Time   `json:"updated_at"`
	DeletedAt    time.Time   `json:"deleted_at"`
	ContactName  string      `json:"contact_name"`
	BusinessName string      `json:"business_name"`
	Email        string      `json:"email"`
	PhoneNumber  string      `json:"phone_number"`
	Address      string      `json:"address"`
	JoinDate     time.Time   `json:"join_date"`
	TypeId       uint        `json:"type_id"`
	RouteId      uint        `json:"route_id"`
	Status       uint        `json:"status"`
}
