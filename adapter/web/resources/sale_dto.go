package resources

import (
	"time"
	models "bitbucket.org/laverita/interecommerce/domain"
)

type (
	SaleListDto struct {
		SaleID		uint		`json:"sale_id"`
		CustomerName	string		`json:"customer_name"`
		Amount		float64		`json:"amount"`
		Date 		time.Time	`json:"date"`
	}

	SaleItemDto struct {
		ID 		uint		`json:"id"`
		SaleID 		uint		`json:"sale_id"`
		ProductName	string		`json:"product_name"`
		UnitPrice 	float64		`json:"unit_price"`
		Quantity	uint		`json:"quantity"`

	}

	MakeSaleInput struct {
		CustomerId	uint		`json:"customer_id"`
		StaffId		uint		`json:"staff_id"`
		WarehouseId	uint		`json:"warehouse_id"`

		SaleItems 	[]models.SalesItem `json:"sale_items"`
	}

	MakeSalesOutput struct {
		Id 		uint        	`json:"id"`
		InvoiceNumber 	string		`json:"invoice_number"`
		Error 		string
	}
)
