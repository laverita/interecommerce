package web

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"fmt"
	"reflect"
	"errors"
	"github.com/labstack/echo"
	"bitbucket.org/laverita/interecommerce/adapter/web/resources"
	"strings"
)

type configuration struct {
	DbHost, DbUserName, DbPassword, Database, Server, Port string
	TokenLifeTime int
}

//Initialize AppConfig
var AppConfig configuration

func initConfig() {
	loadConfig()
}

func loadConfig() {
	file, err := os.Open("common/config.json")
	defer file.Close()
	if err != nil {
		log.Fatalf("[loadConfig]: %s\n", err)
	}
	decoder := json.NewDecoder(file)
	AppConfig = configuration{}
	err = decoder.Decode(&AppConfig)
	if err != nil {
		log.Fatalf("[loadConfig]: %s\n", err)
	}
}

type (
	appError struct {
		Error   string `json:"error"`
		Message string `json:"message"`
		Status  int    `json:"Status"`
	}
	errorResponse struct {
		Data appError `json:"data"`
	}
)

func InvalidRequestResponse(c echo.Context) error {
	return SendError(c, errors.New("Invalid Request"), "Invalid Request")
}

func DisplayAppError(w http.ResponseWriter, handlerError error, message string, code int) {
	errObj := appError{
		Error:   handlerError.Error(),
		Message: message,
		Status:  code,
	}
	log.Printf("[AppError]: %s\n", handlerError)
	w.Header().Set("Content-Type", "application.json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	if j, err := json.Marshal(errorResponse{Data: errObj}); err == nil {
		w.Write(j)
	}
}


func SendError(context echo.Context, handlerError error, messages ...string) error {
	code := http.StatusOK
	var message string
	if len(message) == 0 {
		message = handlerError.Error()
	}else{
		message = strings.Join(messages, ". ")
	}
	errObj := struct{

		Error string	`json:"error"`
		Message string	`json:"message"`
		HttpStatus int `json:"http_status"`
	}{

		Error:      handlerError.Error(),
		Message:    message,
		HttpStatus: code,
	}

	return context.JSON(code, errObj)
}


func SendSuccess(c echo.Context, statusCode int, data interface{}, message ...string) error {
	var token string
	if t := c.Get("token"); t != nil{
		token = t.(string)
	}
	return c.JSON(http.StatusOK, resources.ResponseResource{Data:data, StatusCode:statusCode,
		Success:true, Message:strings.Join(message, ". "), Token: token})
}

func Log(key string, data interface{})  {
	fmt.Printf("%s: %v\n", key, data)
}


func SetField(obj interface{}, name string, value interface{}) error {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)

	if !structFieldValue.IsValid() {
		return fmt.Errorf("No such field: %s in obj", name)
	}

	if !structFieldValue.CanSet() {
		return fmt.Errorf("Cannot set %s field value", name)
	}

	structFieldType := structFieldValue.Type()
	val := reflect.ValueOf(value)
	if structFieldType != val.Type() {
		invalidTypeError := errors.New("Provided value type didn't match obj field type")
		return invalidTypeError
	}

	structFieldValue.Set(val)
	return nil
}

func FillStruct(s interface{}, m map[string]interface{}) error {
	for k, v := range m {
		err := SetField(s, k, v)
		if err != nil {
			return err
		}
	}
	return nil
}


