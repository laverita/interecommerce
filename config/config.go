package config

import (
	"os"

	"github.com/jinzhu/configor"
)

type SMTPConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Site     string
}

var Config = struct {
	Port uint `default:"8000" env:"PORT"`
	DB   struct {
		Name     string `env:"DBName" default:"dms"`
		Adapter  string `env:"DBAdapter" default:"mysql"`
		Host     string `env:"DBHost" default:"localhost"`
		Port     string `env:"DBPort" default:"3306"`
		User     string `env:"DBUser"`
		Password string `env:"DBPassword"`
	}
	SMTP   SMTPConfig
	Env 	string 	`env:"Env" default:"dev"`
}{}

var (
	Root = os.Getenv("GOPATH") + "/src/bitbucket.org/laverita/interecommerceweb"

)

func init() {
	if err := configor.Load(&Config, "config/database.yml", "config/smtp.yml", "config/application.yml"); err != nil {
		panic(err)
	}

	/*View = render.New()

	htmlSanitizer := bluemonday.UGCPolicy()
	View.RegisterFuncMap("raw", func(str string) template.HTML {
		return template.HTML(htmlSanitizer.Sanitize(str))
	})*/
}

func (s SMTPConfig) HostWithPort() string {
	return s.Host + ":" + s.Port
}
