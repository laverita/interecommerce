package domain

import (
	"github.com/ademuanthony/gorepo"
	"reflect"
	"time"
)

type IModel interface {
	gorepo.IModel
	GetName() string
}

type BaseModel struct {
	Id 		uint			`json:"id"`
	CreatedAt 	time.Time		`json:"created_at"`
	UpdatedAt 	time.Time		`json:"updated_at"`
	DeletedAt 	time.Time		`json:"deleted_at"`
}

func (m BaseModel) GetName() string {
	return reflect.TypeOf(m).Name()
}

/*
type Date struct {
	time.Time
}

func (d *Date) UnmarshalJSON(b []byte) (err error) {
	s := string(b)

	// Get rid of the quotes "" around the value.
	// A second option would be to include them
	// in the date format string instead, like so below:
	//   time.Parse(`"`+time.RFC3339Nano+`"`, s)
	s = s[1:len(s)-1]

	t, err := time.Parse(time.RFC3339Nano, s)
	if err != nil {
		t, err = time.Parse("2006-01-02T15:04:05Z07:00", s + "T15:04:05Z07:00")
	}
	d.Time = t
	return errors.New(s + "T15:04:05Z07:00")
}*/
