package domain

import (
	"github.com/jinzhu/gorm"
	"time"
	"github.com/go-ozzo/ozzo-validation"
	"bitbucket.org/superfluxteam/cruder"
	"fmt"
)

// Create a GORM-backend model
type Staff struct {
	gorm.Model
	DepartmentId uint		`json:"department_id"`
	Name         string		`json:"name"`
	Password     string		`json:"-"`
	Gender       string          	`json:"gender" form_field_type:"select_one" form_field_options:"Male:Male,Female:Female"`
	Email        string		`json:"email"`
	PhoneNumber  string		`json:"phone_number"`
	Address      string		`json:"address"`
	StaffNumber  string		`gorm:"unique_index" json:"staff_number"`
	JoinDate     time.Time		`json:"join_date"`
	StatusId     uint		`json:"status_id"`

	Department			Department 	`json:"department"`
	Status 				Status		`gorm:"ForeignKey:StatusId" json:"status"`
	Roles      			[]Role 		`gorm:"many2many:staff_role_map;" json:"roles"`
}


func (s Staff) GetPermissions(context *cruder.Context) []int {

	return make([]int, 0) //fetch the permission
}

func (s Staff) GetRoles(context *cruder.Context) (roles []string) {
	if len(s.Roles) == 0 {
		err := context.DB.Preload("Roles").Find(&s, s.ID).Error
		if err != nil {
			fmt.Println(err)
		}
	}
	for _, role := range s.Roles {
		roles = append(roles, role.Name)
	}
	return
}

func (s Staff) GetId() uint {
	return s.ID
}

func (s Staff) Validate() error {
	return validation.ValidateStruct(&s,
		validation.Field(&s.Name, validation.Required),
		validation.Field(&s.StaffNumber, validation.Required),
	)
}

func (s Staff) Display() string {
	return s.Name
}

func (s Staff) DisplayName() string {
	return s.Name
}
