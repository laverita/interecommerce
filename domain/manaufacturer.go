package domain

import (
	"github.com/jinzhu/gorm"
	"github.com/go-ozzo/ozzo-validation"
)

type Manufacturer struct {
	gorm.Model

	Name     string		`json:"name"`
	StatusId uint		`json:"status_id"`

	//BelongTo
	Status 				Status		`gorm:"ForeignKey:StatusId"`


	//Brands 				[]Brand			`gorm:"ForeignKey:ManufacturerID"`
	//Supplys				[]Supply		`gorm:"ForeignKey:ManufacturerID"`
}

func (m Manufacturer) GetId() uint {
	return m.ID
}

func (m Manufacturer) Validate() error {
	return validation.ValidateStruct(&m, validation.Field(&m.Name, validation.Required))
}