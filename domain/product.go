package domain

import (
	"github.com/jinzhu/gorm"
	"github.com/go-ozzo/ozzo-validation"
)

// Create another GORM-backend model
type Product struct {
	gorm.Model

	Name        string		`json:"name"`
	Description string		`json:"description"`
	Price       float64		`json:"price"`

	BrandID 		uint		`json:"brand_id"`
	Sku 			string		`json:"sku"`

	KongaId			string		`json:"konga_id"`
	JumiaId			string 		`json:"jumia_id"`
	ObyMartId		string		`json:"oby_mart_id"`

	//BelongTo
	Brand			Brand		`gorm:"ForeignKey:BrandID" json:"brand"`
	Stocks 			[]Stock		`json:"stocks"`


	//Details 			[]ProductDetail			`gorm:"ForeignKey:ProductId"`
	//StockRequestItems 	[]StockRequestItem		`gorm:"ForeignKey:ProductId"`
	//StockReturnItem		[]StockReturnItem		`gorm:"ForeignKey:ProductId"`
}

func (p Product) GetId() uint {
	return p.ID
}

func (p Product) Validate() error {
	return validation.ValidateStruct(&p, validation.Field(&p.Name, validation.Required), validation.Field(&p.Sku, validation.Required))
}

func (p Product) Display() string {
	return p.Name
}