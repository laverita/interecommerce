package domain

import (
	"github.com/go-ozzo/ozzo-validation"
	"github.com/jinzhu/gorm"
)

type City struct {
	gorm.Model
	StateId 		uint		`json:"state_id"`
	Name 			string		`json:"name"`

	//Belong To
	State 			State 		`gorm:"ForeignKey:StateId"`
}

func (c City) GetId() uint {
	return c.ID
}

func (c City) Validate() error {
	return validation.ValidateStruct(&c, validation.Field(&c.Name, validation.Required),
		validation.Field(&c.StateId, validation.Required))
}

func (c City) Display() string {
	return c.Name
}