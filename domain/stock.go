package domain


import (
	"github.com/jinzhu/gorm"
	// "time"
	//"github.com/go-ozzo/ozzo-validation"
)

// Create a GORM-backend model
type Stock struct {
	gorm.Model
	Product		Product		`gorm:"ForeignKey:ProductId" json:"product"`
	ProductId       uint		`json:"product_id"`
	Quantity        uint		`json:"quantity"`
}

func (s Stock) GetId() uint {
	return s.ID
}

func (s Stock) Validate() error {
	return nil
	/*return validation.ValidateStruct(&s,
		validation.Field(&s.Quantity, validation.Required, validation.Min(1)),
	)*/
}

func (s Stock) GetBalance() uint {
	return uint(s.Quantity)
}

