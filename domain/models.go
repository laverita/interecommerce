package domain

import (
	"github.com/jinzhu/gorm"
	"github.com/go-ozzo/ozzo-validation"
)

type (
	Application struct {
		gorm.Model

		CreatedBy   uint		`json:"created_by"`
		UpdatedBy   uint		`json:"updated_by"`
		Name        string		`json:"name"`

		Description string		`json:"description"`

		Permissions []Permission        `json:"resources"`
	}

	/*User struct {
		gorm.Model
		Username     	string		`json:"username"`
		Password     	string		`json:"password"`
		Email        	string		`json:"email"`
		FirstName    	string		`json:"first_name"`
		LastName     	string		`json:"last_name"`

		Roles 		[]Role		`json:"roles" gorm:"many2many:user_roles"`
	}*/

	Role struct {
		gorm.Model
		Name        string		`json:"name"`

		Staffs      []Staff 		`gorm:"many2many:staff_role_map;" json:"staffs"`
		Permissions []Permission        `gorm:"many2many:role_permissions" json:"permissions"`
	}
/*
	StaffRole struct {
		gorm.Model
		RoleId    uint		`json:"role_id"`
		StaffId   uint		`json:"staff_id"`
		CreatedAt time.Time	`json:"created_date"`
		CreatedBy uint		`json:"created_by"`

		Role  Role		`json:"role" gorm:"ForeignKey:RoleId"`
		Staff Staff		`json:"staff" gorm:"ForeignKey:StaffId"`
	}*/
)

func (a Application) GetId() uint {
	return a.ID
}

func (a Application) Validate() error {
	return validation.ValidateStruct(&a, validation.Field(&a.Name, validation.Required))
}

func (r Role) GetId() uint {
	return r.ID
}

func (r Role) Validate() error {
	return validation.ValidateStruct(&r, validation.Field(&r.Name, validation.Required))
}