package domain

import (
	"github.com/jinzhu/gorm"
	"github.com/go-ozzo/ozzo-validation"
)

// Create a GORM-backend model
type BankAccount struct {
	gorm.Model
	BankID					uint	`json:"bank_id"`
	Name					string	`json:"name"`
	Number					string	`json:"number"`
	Address					string	`json:"address"`

	Bank					Bank	`json:"bank"`
}

func (b BankAccount) GetId() uint {
	return b.ID
}

func (b BankAccount) Validate() error {
	return validation.ValidateStruct(&b, validation.Field(&b.Name, validation.Required), validation.Field(&b.Number, validation.Required),
	validation.Field(&b.BankID, validation.Required))
}

func (b BankAccount) Display() string {
	return b.Name + " " + b.Number
}