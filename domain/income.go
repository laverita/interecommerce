package domain

import (
	"github.com/go-ozzo/ozzo-validation"
	"github.com/jinzhu/gorm"
	"fmt"
	"time"
)

type Income struct {
	gorm.Model
	BankAccountId uint		`json:"bank_account_id"`
	Narration     string		`json:"narration"`
	Amount        uint		`json:"amount"`
	Date          time.Time	`json:"date"`

	BankAccount 	BankAccount	`json:"bank_account"`
}

func (i Income) GetId() uint {
	return i.ID
}

func (i Income) Validate() error {
	return validation.ValidateStruct(&i, validation.Field(&i.BankAccountId, validation.Required))
}

func (i Income) Display() string {
	return fmt.Sprintf("%f", i.Amount)
}