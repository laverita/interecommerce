package domain

import (
	"github.com/go-ozzo/ozzo-validation"
	"github.com/jinzhu/gorm"
)

type State struct {
	gorm.Model
	Name			string 		`gorm:"type:varchar(100);unique_index"`
	Capital			string

	Cities 			[]City		`gorm:"ForeignKey:StateId"`
}

func (s State) GetId() uint {
	return s.ID
}

func (s State) Validate() error {
	return validation.ValidateStruct(&s, validation.Field(&s.Name, validation.Required))
}

func (s State) Display() string {
	return s.Name + " " + s.Capital
}