package domain

import (
	"github.com/jinzhu/gorm"
	"github.com/go-ozzo/ozzo-validation"
)

type (
	Permission struct {
		gorm.Model
		Name string		`json:"name"`

		Roles 			[]Role	`gorm:"many2many:role_permissions" json:"roles"`
	}



	RolePermission struct {
		gorm.Model

		RoleId       uint
		PermissionId uint

		Permission Permission
		Role     Role
	}
)

func (p Permission) GetId() uint {
	return p.ID
}

func (p Permission) Validate() error {
	return validation.ValidateStruct(&p, validation.Field(&p.Name, validation.Required))
}

func (rp RolePermission) GetId() uint {
	return rp.ID
}

func (rp RolePermission) Validate() error {
	return validation.ValidateStruct(&rp, validation.Field(&rp.PermissionId, validation.Required),
	validation.Field(&rp.RoleId, validation.Required))
}
