package domain


import (
	"github.com/jinzhu/gorm"
	"time"
	"github.com/go-ozzo/ozzo-validation"
)

// Create a GORM-backend model
type Sale struct {
	gorm.Model
	Market 						Market			`json:"market"`
	MarketId					uint                    `json:"market_id"`
	StaffId						uint			`json:"staff_id"`
	StatusID					uint			`json:"status_id"`
	Date						time.Time		`json:"date"`
	OrderId						uint			`json:"order_id"`
	Order						Order			`json:"order"`
	Amount 						float64			`json:"amount"`
	Name 						string                  `json:"name"`
	//BelongTo
	Staff						Staff			`gorm:"ForeignKey:StaffId" json:"staff"`
	Status						Status			`gorm:"ForeignKey:StatusID" json:"status"`


}

func (s Sale) GetId() uint {
	return s.ID
}

func (s Sale) Validate() error {
	return validation.ValidateStruct(&s, validation.Field(&s.StaffId, validation.Required))
}

func (s Sale) Display() string {
	return s.Name
}

func (s Sale) GetTotalAmount() (amount float64) {
	amount = s.Amount
	return
}


