package domain

import (
	"github.com/jinzhu/gorm"
	"time"
	"github.com/go-ozzo/ozzo-validation"
	"fmt"
)

type Payment struct {
	gorm.Model

	SaleId uint	`json:"sale_id"`
	Amount float64	`json:"amount"`
	Date time.Time	`json:"date"`
	StatusId uint	`json:"status_id"`

	Status Status	`json:"status"`
	Sale	Sale	`json:"sale"`
}

func (p Payment) GetId() uint {
	return p.ID
}

func (p Payment) Validate() error {
	return validation.ValidateStruct(&p, validation.Field(&p.Amount, validation.Required))
}

func (p Payment) Display() string {
	return fmt.Sprintf("%f", p.Amount)
}

