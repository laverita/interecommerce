package domain


import (
	//"github.com/jinzhu/gorm"
)
import (
	"github.com/go-ozzo/ozzo-validation"
	"github.com/jinzhu/gorm"
)

// Create a GORM-backend model

type Status struct {
	gorm.Model
	Name 					string
}

func (s Status) Display() string {
	return s.Name
}

func (s Status) GetId() uint {
	return s.ID
}

func (s Status) Validate() error {
	return validation.ValidateStruct(&s, validation.Field(&s.Name, validation.Required))
}



const (
	StatusUnknown            = 0
	StatusActive             = 1
	StatusInActive           = 2
	StatusOrderNew           = 3
	StatusOrderShipped       = 4
	StatusOrderCancelled     = 5
	StatusOrderReturned      = 6
	StatusOrderDelivered     = 7
	ReturnedOrderReceived	 = 8

	PaymentStatusPending	= 9
	PaymentStatusReceived	= 10
)