package domain

import (
	"time"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/jinzhu/gorm"
)

type Expenditure struct {
	gorm.Model
	Amount 		float64		`json:"amount"`
	Date 		time.Time	`json:"date"`
	Narration 	string		`json:"narration"`
	BankAccountId 	uint		`json:"bank_account_id"`

	BankAccount 	BankAccount	`json:"bank_account"`
}

func (e Expenditure) GetId() uint {
	return e.ID
}

func (e Expenditure) Validate() error {
	return validation.ValidateStruct(&e, validation.Field(&e.BankAccountId, validation.Required))
}
