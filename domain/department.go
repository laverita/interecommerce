package domain

import (
	"github.com/jinzhu/gorm"
	"github.com/go-ozzo/ozzo-validation"
)

type Department struct {
	gorm.Model
	Name	string	`json:"name"`
}

func (d Department) GetId() uint {
	return d.ID
}

func (d Department) Validate() error {
	return validation.ValidateStruct(&d, validation.Field(&d.Name, validation.Required))
}

func (d Department) Display() string {
	return d.Name
}
