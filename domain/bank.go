package domain

import (
	"github.com/jinzhu/gorm"
	"github.com/go-ozzo/ozzo-validation"
)

type Bank struct {
	gorm.Model
	Name					string		`json:"name" gorm:"unique_index"`

	//HasMany
	BankAccounts 				[]BankAccount 	`gorm:"ForeignKey:BankID" json:"bank_accounts"`
}

func (b Bank) GetId() uint {
	return b.ID
}

func (b Bank) Validate() error {
	return validation.ValidateStruct(&b, validation.Field(&b.Name, validation.Required))
}

func (b Bank) Display() string {
	return b.Name
}