package domain

import (
	"github.com/jinzhu/gorm"
	"github.com/go-ozzo/ozzo-validation"
)

type Market struct {
	gorm.Model
	Name 	string	`json:"name"`
	Username string	`json:"username"`
	Password string	`json:"password"`
}

func (m Market) GetId() uint {
	return m.ID
}

func (m Market) Validate() error {
	return validation.ValidateStruct(&m, validation.Field(&m.Name, validation.Required))
}

func (m Market) Display() string {
	return m.Name
}

var (
	MARKET_KONGA uint = 1
	MARKET_JUMIA = 2
	MARKET_WORD_PRESS = 3
)