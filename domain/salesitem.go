package domain


import (
	"github.com/jinzhu/gorm"
)

// Create a GORM-backend model
type SalesItem struct {
	gorm.Model
	SalesID			uint		`json:"sales_id"`
	ProductID		uint		`json:"product_id"`
	StockID			uint		`json:"stock_id"`
	Quantity		uint		`json:"Quantity"`
	UnitPrice		float64		`json:"unit_price"`

	//BelongTo
	Product			Product		`gorm:"ForeignKey:ProductID"`

	//SalesReturns				[]SalesReturn	`gorm:"ForeignKey:SaleItemID"`
}

