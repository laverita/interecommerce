package domain

import (
	"github.com/jinzhu/gorm"
	"github.com/go-ozzo/ozzo-validation"
)

type Brand struct {
	gorm.Model

	Name 				string		`json:"name"`
	ManufacturerId		uint			`json:"manufacturer_id"`

	Manufacturer		Manufacturer		`json:"manufacturer"`
}

func (b Brand) GetId() uint {
	return b.ID
}

func (b Brand) Validate() error {
	return validation.ValidateStruct(&b, validation.Field(&b.Name, validation.Required),
		validation.Field(&b.ManufacturerId, validation.Required))
}