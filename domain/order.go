package domain

import (
	"github.com/jinzhu/gorm"
	"fmt"
)

type Order struct {
	gorm.Model
	Market		Market	`json:"market"`
	MarketId	uint	`json:"market_id"`
	RefId		string	`json:"ref_id"`
	StatusId	uint	`json:"status_id"`
	Status		Status	`json:"status"`

	Items		[]OrderItem `json:"items"`
	OrderHistories 	[]OrderHistory `json:"order_histories"`
}

func (o Order) GetId() uint {
	return o.ID
}

func (o Order) Validate() error {
	return nil
}

func (o Order) Display() string {
	return fmt.Sprintf("Order #%d, Ref: %s, Market: %s", o.ID, o.RefId, o.Market.Display())
}


type OrderItem struct {
	gorm.Model
	Product		Product		`json:"product"`
	Order		Order		`json:"order"`
	OrderId 	uint		`json:"order_id"`
	ProductId 	uint	`json:"product_id"`
	Quantity	uint	`json:"quantity"`
	UnitPrice	float64	`json:"unit_price"`
}

func (o OrderItem) GetId() uint {
	return o.ID
}

func (o OrderItem) Validate() error  {
	return nil
}

type OrderHistory struct {
	gorm.Model
	OrderId uint	`json:"order_id"`
	StatusId uint `json:"status_id"`
	Order Order `json:"order"`
	Status Status `json:"status"`
}

func (o OrderHistory) GetId() uint {
	return o.ID
}

func (o OrderHistory) Validate() error  {
	return nil
}