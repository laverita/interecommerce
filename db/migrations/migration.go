package migrations

import (
	. "bitbucket.org/laverita/interecommerce/domain"
	"bitbucket.org/laverita/interecommerce/db"
	"bitbucket.org/superfluxteam/cruder/navigation"
)

func init() {
	//AutoMigrate(&StaffRole{})
	AutoMigrate(/*&User{},*/ &Application{}, &Permission{}, &Role{}, &RolePermission{}, )
	AutoMigrate(&navigation.Menu{})
	AutoMigrate(&Status{}, &Department{}, &Staff{}, &Product{}, &Manufacturer{}, &Brand{})
	AutoMigrate(&State{}, &City{})
	AutoMigrate(&Stock{},)
	AutoMigrate(&Bank{},)
	AutoMigrate(&BankAccount{},)
	AutoMigrate(&Brand{},)

	AutoMigrate(&Market{}, &Order{}, &OrderItem{}, &OrderHistory{}, &Sale{}, &SalesItem{}, &Payment{})
	AutoMigrate(&Income{}, &Expenditure{})
}

func AutoMigrate(values ...interface{}) {
	for _, value := range values {
		db.GetDb().AutoMigrate(value)
	}
}
