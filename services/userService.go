package services

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	models "bitbucket.org/laverita/interecommerce/domain"
	"gopkg.in/hlandau/passlib.v1"
)

type UserService struct {
	Db *gorm.DB
}

func (u *UserService) Login(username, password string) (models.Staff, error) {
	var staff  models.Staff
	if err := u.Db.Where(&models.Staff{Email:username}).Find(&staff).Error; err != nil {
		if err = u.Db.Where(&models.Staff{StaffNumber:username}).Find(&staff).Error; err != nil{
			return staff, errors.New("Invalid credentials")
		}
	}

	/*
	newHash, err := passlib.Verify(password, staff.Password)
	*/

	if password != staff.Password /*err != nil*/ {
		/*pass, _ := passlib.Hash(password)
		staff.Password = pass
		u.Db.Save(staff)*/
		// app.Logger.Error(err)
		return models.Staff{}, errors.New("Invalid credentials")
	}

	/*if newHash != ""{
		staff.Password = newHash
		u.Db.Update(&staff)
	}*/

	return staff, nil
}

func (u *UserService) ChangePassword(userId uint, oldPassword, newPassword string) error {
	var user models.Staff
	err := u.Db.First(&user, userId).Error
	if err != nil{
		return errors.New("Staff not found")
	}

	_, err = passlib.Verify(oldPassword, user.Password)

	if err != nil {
		return errors.New("Invalid credentials")
	}

	hashedPassword, err := passlib.Hash(newPassword) // bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	if err != nil {
		fmt.Printf("NUM: ERR: %v\n", err)
	}

	user.Password = string(hashedPassword)
	u.Db.Save(&user)
	return nil
}

func (u *UserService) ChangePasswordForId(userId uint, newPassword string) error {
	var user models.Staff

	err := u.Db.First(&user, userId).Error
	if err != nil{
		return errors.New("Staff not found")
	}

	hashedPassword, err := passlib.Hash(newPassword) // bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	if err != nil {
		fmt.Printf("NUM: ERR: %v\n", err)
	}

	user.Password = string(hashedPassword)
	u.Db.Save(&user)
	return nil
}