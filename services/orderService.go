package services

import (
	"bitbucket.org/laverita/interecommerce/domain"
	"github.com/jinzhu/gorm"
)

func CreateOrder(order domain.Order, db *gorm.DB) (orderId uint, err error) {
	err = db.Create(&order).Error
	return
}

func CreateOrderHistory(orderId, statusId uint, db *gorm.DB) (history domain.OrderHistory, err error) {
	history.StatusId = statusId
	history.OrderId = orderId
	err = db.Create(history).Error
	return
}
