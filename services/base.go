package services

import (
	"github.com/ademuanthony/goinject"
	"github.com/ademuanthony/gorepo"
	"github.com/jinzhu/gorm"
	"bitbucket.org/laverita/interecommerce/di"
	"bitbucket.org/laverita/interecommerce/providers"
	"bitbucket.org/laverita/interecommerce/app"
)

type base struct {
	TransactionManager providers.ITransactionManager
	DiManager goinject.IManager
	repository gorepo.IRepository
	db *gorm.DB
}

func (b *base) Initialize(args ...interface{})  {
	if len(args) != 2 {
		panic("base service initialization expects two args")
	}
	if manager, ok := args[0].(goinject.IManager); ok {
		b.DiManager = manager
	}else{
		panic("The first arg must be goinject.IManager")
	}

	if db, ok := args[1].(*gorm.DB); ok {
		b.db = db
	}else{
		panic("The second arg must be *gorm.DB")
	}

}

func (b base) ResolveDependency(key goinject.DiKey, args *gorm.DB) goinject.IInjectable {
	if b.DiManager == nil{
		panic("DiManager cannot be nil")
	}
	obj, _ := b.DiManager.Resolve(key, args)
	return obj
}


func (b *base) getRepository() gorepo.IRepository {
	if b.repository == nil{
		repo, err := b.DiManager.Resolve(di.Repository, b.db)
		if err != nil{
			app.Logger.Error(err)
			panic(err)
		}
		b.repository = repo.(gorepo.IRepository)
	}
	return b.repository
}

func (b *base) BeginTransaction() {
	b.db = b.db.Begin()
}

func (b *base) CommitTransaction() {
	b.db.Commit()
}

func (b *base) RollbackTransaction() {
	b.db.Rollback()
}