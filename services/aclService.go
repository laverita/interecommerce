package services

import (
	"errors"
	"fmt"
	"strings"
	"github.com/jinzhu/gorm"
	models "bitbucket.org/laverita/interecommerce/domain"
	"github.com/qor/roles"
	"bitbucket.org/laverita/interecommerce/db"
	"github.com/ahmetb/go-linq"
)

type AclService struct {
	Db *gorm.DB
}

func (a *AclService) CreatePermission(permission models.Permission) (models.Permission, error) {
	if !a.Db.NewRecord(&models.Permission{Name: permission.Name}) {
		return permission, errors.New("Permission with the same key already exists")
	}
	err := a.Db.Create(&permission).Error

	return permission, err
}

func (a *AclService) UpdatePermission(resource models.Permission) error {
	return a.Db.Save(&resource).Error
}

func (a *AclService) GetPermissionById(id uint) (models.Permission, error) {
	var resource models.Permission
	err := a.Db.First(&resource, id).Error

	return resource, err
}

func (a *AclService) DeletePermission(id uint) (string, error) {
	//remove u resource from all roles
	a.Db.Where(&models.RolePermission{PermissionId:id}).Delete(&models.RolePermission{})

	err := a.Db.Delete(&models.Permission{}, "id = ?", id).Error
	if err != nil{
		return "Error in delete resource", err
	}
	return "Permission Deleted", nil
}

func (a *AclService) GetPermissionByName(name string) (models.Permission, error) {
	var resource models.Permission
	err := a.Db.Where(&models.Permission{Name:name}).First(&resource).Error

	return resource, err
}

func (a *AclService) GetAllPermissions() ([]models.Permission, error) {
	var permissions []models.Permission
	err := a.Db.Find(&permissions).Error

	return permissions, err
}

// Create a role in the Db
func (a AclService) CreateRole(role models.Role) (models.Role, error) {
	if !a.Db.NewRecord(&models.Role{Name: role.Name}){
		return role, errors.New("A role with the same name already exists")
	}
	err := a.Db.Create(&role).Error
	return role, err
}

func (a AclService) GetRoles() []models.Role {
	var roles []models.Role
	a.Db.Find(&roles)
	return roles
}

func (a AclService) GetRoleByName(name string) (models.Role, error){
	var role models.Role
	err := a.Db.Where("name = ?", name).Find(&role).Error
	return role, err
}

func (a AclService) GetRoleById(id uint) (models.Role, error) {
	var role models.Role
	err := a.Db.First(&role, id).Error
	return role, err
}

func (a AclService) DeleteRole(roleId uint) error {
	// remove all resources from u role
	a.Db.Where("role_id = ?", roleId).Delete(&models.RolePermission{})
	err := a.Db.Where("id = ?", roleId).Delete(&models.Role{}).Error;
	return err;
}

// Role resource
func (a *AclService) AddPermissionToRole(resourceId uint, roleId uint) (uint, error) {
	rolePermission := models.RolePermission{RoleId: roleId, PermissionId: resourceId}

	//don't add a resource to a role twice
	if !a.Db.NewRecord(&models.RolePermission{RoleId: roleId, PermissionId: resourceId}){
		return 0, errors.New("The selected resource is already in u role")
	}

	err := a.Db.Create(&rolePermission).Error
	if err != nil{
		return 0, err
	}
	return rolePermission.ID, err
}

func (a *AclService) AddPermissionsToRole(permissionIds []uint, roleId uint) error {
	commaSeparatedPermissionList := arrayToString(permissionIds, ",")

	var permissions []models.Permission
	err := a.Db.Table("permissions").Select("permissions.id").Joins("INNER JOIN role_permissions ON " +
		"permissions.id = role_permissions.permission_id").Where("role_permissions.role_id = ? AND" +
		" permissions.resource_id NOT IN (?)", roleId, commaSeparatedPermissionList).Find(&permissions).Error

	if err != nil{
		return err
	}

	for _, permissionToDelete := range permissions {
		a.RemovePermissionFromRole(permissionToDelete.ID, roleId)
	}

	for _, permissionId := range permissionIds {

		if a.Db.NewRecord(&models.RolePermission{RoleId: roleId, PermissionId: permissionId}) {
			a.AddPermissionToRole(permissionId, roleId)
		}
	}

	return nil
}

func arrayToString(a []uint, delimiter string) string {
	return strings.Trim(strings.Replace(fmt.Sprint(a), " ", delimiter, -1), "[]")
	//return strings.Trim(strings.Join(strings.Split(fmt.Sprint(a), " "), delimiter), "[]")
	//return strings.Trim(strings.Join(strings.Fields(fmt.Sprint(a)), delimiter), "[]")
}

func (a *AclService) GetPermissionsInRole(roleId uint) []models.Permission {
	var role models.Role
	err := a.Db.Preload("Permissions").Find(&role, roleId).Error
	if err != nil{
		return []models.Permission{}
	}

	return role.Permissions
}

func (a *AclService) RemovePermissionFromRole(permissionId uint, roleId uint) error {
	err := a.Db.Where(&models.RolePermission{PermissionId:permissionId, RoleId:roleId}).Delete(&models.RolePermission{}).Error
	return err
}

func (a *AclService) GetPermissionByRoleId(roleId uint) []models.Permission {
	return a.GetPermissionsInRole(roleId)
}

//Staff role
func (a *AclService) AddStaffToRole(staffId uint, roleId uint) error {
	staff := models.Staff{}
	staff.ID = staffId
	if err := a.Db.Preload("Roles").Find(&staff, staff).Error; err != nil {
		return errors.New("Invalid staff ID")
	}

	role := models.Role{}
	role.ID = roleId
	if err := a.Db.Find(&role, roleId); err != nil {
		return errors.New("Invalid role ID")
	}
	if found := linq.From(staff.Roles).Where(func(role interface{}) bool {
		return role.(models.Role).ID == roleId
	}).Count(); found > 0 {
		return errors.New("The specified staff is already in the selected role")
	}
	staff.Roles = append(staff.Roles, role)
	return a.Db.Save(&staff).Error
}

func (a *AclService) GetStaffsInRole(roleId int64) []models.Staff {
	var role models.Role
	err := a.Db.Preload("Staffs").Where("role_id = ?", roleId).Find(&role).Error

	if err != nil{
		return []models.Staff{}
	}

	return  role.Staffs
}

func (a *AclService) RemoveStaffFromRole(userId uint, roleId uint) error {
	var staff models.Staff
	if err := a.Db.Preload("Roles").Find(&staff, userId).Error; err != nil {
		return errors.New("Invalid staff ID")
	}
	for i, r := range staff.Roles {
		if r.ID == roleId {
			staff.Roles = append(staff.Roles[:i], staff.Roles[i+1:]...)
		}
	}
	return a.Db.Save(staff).Error
}

func (a *AclService) GetRolesForStaff(userId int64) []models.Role {
	var user models.Staff
	if a.Db.Preload("Roles").Where("id = ?").Find(&user).Error != nil{
		return []models.Role{}
	}
	return user.Roles
}

func (a AclService) StaffIsInRole(staffId, roleId uint) bool {
	var count int
	err := a.Db.Debug().Model(&models.Staff{}).Where("id = ?", staffId).Related("Roles").Where("id = ?", roleId).Count(&count).Error
	if err != nil {
		panic(err) //todo remove the panic
		return false
	}
	return count > 0
}

func (a *AclService) GetPermissionsForStaff(staffId uint) []uint {
	var resources []models.Permission
	a.Db.Table("role_permissions").Select("role_permissions.permission_id AS id").
		Joins("INNER JOIN roles ON role_permissions.role_id = roles.id").
	Joins("INNER JOIN staff_role_map ON staff_role_map.role_id = roles.id").
		Where("staff_role_map.staff_id = ?", staffId).Find(&resources)

	ids := make([]uint, len(resources))
	for index, resource := range resources{
		ids[index] = resource.ID
	}

	return ids
}

var (
	permission *roles.Permission
	DefaultAclService AclService
)

func init() {
	permission = roles.Allow(roles.CRUD, "admin")
	DefaultAclService = AclService{Db:db.GetDb()}
}

func (a AclService) HasPermission(resource models.IModel, permissionMode roles.PermissionMode, role ...string) bool {
	return true // permission.HasPermission(permissionMode, role...)
}