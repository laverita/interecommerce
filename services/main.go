package services

import (
	"github.com/ademuanthony/goinject"
	"github.com/ademuanthony/gorepo"
	"reflect"
	"bitbucket.org/laverita/interecommerce/di"
)

func NewDefaultDiManager() goinject.IManager {
	manager := goinject.NewDefaultDiManager(
		goinject.Definition{Key:di.Repository, Type:reflect.TypeOf(gorepo.GormRepository{})},

	)

	return manager
}
