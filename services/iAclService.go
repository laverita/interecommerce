package services

import (
	models "bitbucket.org/laverita/interecommerce/domain"
	"github.com/qor/roles"
)

type IAclService interface {
	HasPermission(resource models.IModel, permissionMode roles.PermissionMode, role ...string) bool
}
