package di

import "github.com/ademuanthony/goinject"

const (
	Manufacturer goinject.DiKey = iota
	Brand
	Product
	User
	Repository
	ProductRepository
	SalesRepRepository
	StockRepository
	BroadcastRepository
	CustomerRepRepository
	RouteRepository
	SearchRepository
	InvoiceRepository


	BroadcastService
	CustomerRepService
	PeopleService
	InventoryService
	InvoiceService
	SalesService
	StockTransferService
	SearchService

)
