### What is this repository for? ###
This is the backend of the production management system. 
The system uses [clean code architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html)
* Version: 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Run  
- go get github.com/tools/godep  
- godep restore  

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Coding flow ###
* Create machine domain obj in domain package. If CRUD needs domain rules, implement them here
* Create IMachineRepository Interface in domain package
* Create MockMachineRepository in domain package
* Create machineService in service package and create a factory method NewMachineService for the service
* Implement IMachineRepository in providers.db
* Call the NewMachineService in the controller passing the actual implementation 
of machine repository and db.NewTransaction

### Dependencies ###
* github.com/tools/godep
* github.com/labstack/echo/...
* github.com/ahmetb/go-linq
* github.com/rubenv/sql-migrate
* github.com/go-ozzo/ozzo-validation
* go get github.com/pilu/fresh

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact