package apperrors

const (
	MachineRecordNotFound          = "Record Not Found"
	MachineDetailsTitleEmpty       = "Machine details title cannot be empty"
	MachineDetailsValueEmpty       = "Machine details value cannot be empty"
	CreatedByIDNotZero             ="Created by id cannot be zero"
	MachineFormTitleEmptyMachineID = "Machine id cannot be zero"
	MachineFormFormID              = "Form id cannot be zero"
	MachineFormCreatedByID         = "Created by id cannot be zero"
	MachineFormEmptyId             = "Id cannot be zero"
	MachineIDNotZero               = "Machine ID cannot be zero"
	MachineLocationIDNotZero       = "Machine location cannot be zero"
	MachineOperationIDNotZero      = "Operation ID cannot zero"
	IDCannotBeZero = "Id cannot be zero"
)
