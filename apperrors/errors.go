package apperrors

import "errors"

// apperrors.New create a new error with the supplied message
func New(errorMessage string) error {
	return errors.New(errorMessage)
}

const (
	RecordNotFound                = "Record Not Found"


	MachineControlNumberDuplicate = "A machine with the same control number have been added"
	MachineControlNumberEmpty = "Machine control number cannot be empty"
	MachineNameEmpty = "Machine name cannot be empty"
	RequiredFieldsNotSupplied = "Required fields not supplied."

	FormNameDuplicate = "A form with the save name exists"

	FormWizardExists = "A wizard with the same section name already exists in this form"
)
