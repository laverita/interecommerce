
-- +migrate Up

CREATE TABLE `pms_form_template` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`description` VARCHAR(500) NOT NULL DEFAULT '',
	`create_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` INT NOT NULL,
	`updated_by` INT NOT NULL,
	`update_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

ALTER TABLE `pms_template`
	ADD CONSTRAINT `FK_pms_template_pms_staff` FOREIGN KEY (`created_by`) REFERENCES `pms_staff` (`id`),
	ADD CONSTRAINT `FK_pms_template_pms_staff_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_staff` (`id`);

ALTER TABLE `pms_form`
	ALTER `title` DROP DEFAULT;
ALTER TABLE `pms_form`
	ADD COLUMN `template_id` INT(11) NOT NULL AFTER `id`,
	CHANGE COLUMN `title` `title` VARCHAR(255) NOT NULL AFTER `template_id`,
	ADD COLUMN `name` VARCHAR(255) NOT NULL AFTER `title`,
	CHANGE COLUMN `description` `description` VARCHAR(50) NOT NULL DEFAULT '' AFTER `name`,
	CHANGE COLUMN `instruction` `instruction` TEXT NOT NULL DEFAULT '' AFTER `description`,
	CHANGE COLUMN `solf_deleted` `is_solf_deleted` TINYINT NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted' AFTER `updated_by_id`,
	CHANGE COLUMN `status` `status` TINYINT NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active' AFTER `is_solf_deleted`,
	DROP COLUMN `attribute_name`,
	DROP COLUMN `attribute_id`,
	DROP COLUMN `form_type`,
	DROP COLUMN `form_style_id`,
	ADD UNIQUE INDEX `name` (`name`),
	ADD CONSTRAINT `FK_pms_form_pms_template` FOREIGN KEY (`template_id`) REFERENCES `pms_template` (`id`);

CREATE TABLE `pms_form_property` (
	`id` INT NOT NULL,
	`form_id` INT NOT NULL,
	`name` VARCHAR(50) NOT NULL,
	`value` VARCHAR(500) NOT NULL DEFAULT '',
	CONSTRAINT `FK__pms_form` FOREIGN KEY (`form_id`) REFERENCES `pms_form` (`id`)
)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
;

ALTER TABLE `pms_form_field`
	ALTER `form_id` DROP DEFAULT,
	ALTER `title` DROP DEFAULT;
ALTER TABLE `pms_form_field`
	CHANGE COLUMN `form_id` `form_id` INT(11) NULL AFTER `id`,
	ADD COLUMN `wizard_id` INT(11) NOT NULL AFTER `form_id`,
	CHANGE COLUMN `title` `title` VARCHAR(255) NOT NULL AFTER `wizard_id`,
	CHANGE COLUMN `description` `description` TEXT NOT NULL AFTER `title`,
	CHANGE COLUMN `solf_deleted` `is_solf_deleted` SMALLINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted' AFTER `updated_by_id`;

ALTER TABLE `pms_form_field_properties`
	ALTER `field_title` DROP DEFAULT;
ALTER TABLE `pms_form_field_properties`
	CHANGE COLUMN `field_title` `attribute` VARCHAR(255) NOT NULL AFTER `form_field_id`,
	CHANGE COLUMN `field_value` `value` TEXT NOT NULL DEFAULT '' AFTER `attribute`;

drop table IF EXISTS pms_form_style;



-- +migrate Down
