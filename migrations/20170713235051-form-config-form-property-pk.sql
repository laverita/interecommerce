
-- +migrate Up
ALTER TABLE `pms_form_property`
	CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
	ADD PRIMARY KEY (`id`);

ALTER TABLE `pms_form`
	CHANGE COLUMN `is_solf_deleted` `is_soft_deleted` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted' AFTER `updated_by_id`;

ALTER TABLE `pms_form_template`
	ALTER `created_by` DROP DEFAULT,
	ALTER `updated_by` DROP DEFAULT;
ALTER TABLE `pms_form_template`
	CHANGE COLUMN `created_by` `created_by_id` INT(11) NOT NULL AFTER `create_at`,
	CHANGE COLUMN `updated_by` `updated_by_id` INT(11) NOT NULL AFTER `created_by_id`;

ALTER TABLE `pms_form_template`
	ALTER `update_at` DROP DEFAULT;
ALTER TABLE `pms_form_template`
	CHANGE COLUMN `update_at` `updated_at` DATETIME NOT NULL AFTER `updated_by_id`;

ALTER TABLE `pms_form_field`
	CHANGE COLUMN `update_at` `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP AFTER `created_at`,
	CHANGE COLUMN `is_solf_deleted` `is_soft_deleted` SMALLINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted' AFTER `updated_by_id`;

ALTER TABLE `pms_form_field_properties`
	CHANGE COLUMN `update_at` `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP AFTER `created_at`,
	CHANGE COLUMN `solf_deleted` `is_soft_deleted` SMALLINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted' AFTER `updated_by_id`;

-- +migrate Down
