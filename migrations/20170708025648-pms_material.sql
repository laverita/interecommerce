
-- +migrate Up
CREATE TABLE IF NOT EXISTS `pms_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0' COMMENT '0 = not deleted; 1 = deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  `machine_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1_pms_material` (`created_by_id`),
  KEY `FK2_pms_material` (`updated_by_id`),
  KEY `FK3_pms_material` (`machine_id`),
  CONSTRAINT `FK1_pms_material` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  CONSTRAINT `FK2_pms_material` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`),
  CONSTRAINT `FK3_pms_material` FOREIGN KEY (`machine_id`) REFERENCES `pms_machine` (`id`)
);

CREATE TABLE IF NOT EXISTS `pms_material_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0' COMMENT '0 = not deleted; 1 = deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK1_pms_material_detail` (`material_id`),
  KEY `FK2_pms_material_detail` (`created_by_id`),
  KEY `FK3_pms_material_detail` (`updated_by_id`),
  CONSTRAINT `FK1_pms_material_detail` FOREIGN KEY (`material_id`) REFERENCES `pms_material` (`id`),
  CONSTRAINT `FK2_pms_material_detail` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  CONSTRAINT `FK3_pms_material_detail` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`)
);

CREATE TABLE IF NOT EXISTS `pms_operation_qc_check_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operation_id` int(11) NOT NULL,
  `qc_check_group_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0' COMMENT '0 = not deleted; 1 = deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK1_pms_operation_qc_check_group` (`operation_id`),
  KEY `FK2_pms_operation_qc_check_group` (`qc_check_group_id`),
  KEY `FK3_pms_operation_qc_check_group` (`created_by_id`),
  KEY `FK4_pms_operation_qc_check_group` (`updated_by_id`),
  CONSTRAINT `FK1_pms_operation_qc_check_group` FOREIGN KEY (`operation_id`) REFERENCES `pms_operation` (`id`),
  CONSTRAINT `FK2_pms_operation_qc_check_group` FOREIGN KEY (`qc_check_group_id`) REFERENCES `pms_qc_check_group` (`id`),
  CONSTRAINT `FK3_pms_operation_qc_check_group` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  CONSTRAINT `FK4_pms_operation_qc_check_group` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`)
);
-- +migrate Down
