
-- +migrate Up
ALTER TABLE `pms_form`
	ADD COLUMN `type` VARCHAR(100) NOT NULL DEFAULT 'record' AFTER `name`;

-- +migrate Down
