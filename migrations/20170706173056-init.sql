
-- +migrate Up

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2017 at 11:08 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bas`
--

-- --------------------------------------------------------

--
-- Table structure for table `pms_staff`
--

CREATE TABLE `pms_staff` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`staff_no` INT(11) NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `staff_no_uniq_index` (`staff_no`),
	INDEX `staff_no_key_index` (`staff_no`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


--
-- Table structure for table `pms_form`
--

CREATE TABLE IF NOT EXISTS `pms_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `instruction` text,
  `attribute_name` varchar(50) DEFAULT NULL,
  `attribute_id` varchar(50) DEFAULT NULL,
  `form_type` smallint(6) DEFAULT NULL COMMENT 'Plain or Wizard,',
  `form_style_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK__pms_staff_pms_form` (`created_by_id`),
  KEY `FK2__pms_staff_pms_form` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_form_field`
--

CREATE TABLE IF NOT EXISTS `pms_form_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK1__pms_staff_pms_form_field` (`form_id`),
  KEY `FK2__pms_staff_pms_form_field` (`created_by_id`),
  KEY `FK3__pms_staff_pms_form_field` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_form_field_properties`
--

CREATE TABLE IF NOT EXISTS `pms_form_field_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_field_id` int(11) NOT NULL,
  `field_title` varchar(255) DEFAULT NULL,
  `field_value` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK1__pms_staff_pms_form_field_properties` (`form_field_id`),
  KEY `FK2__pms_staff_pms_form_field_properties` (`created_by_id`),
  KEY `FK3__pms_staff_pms_form_field_properties` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_form_style`
--

CREATE TABLE IF NOT EXISTS `pms_form_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_form_style_user_created` (`created_by_id`),
  KEY `FK_form_style_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_form_wizard`
--

CREATE TABLE IF NOT EXISTS `pms_form_wizard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL COMMENT 'section name can be used for id attr e.g. Step One(step_one)',
  `class` varchar(100) NOT NULL COMMENT 'class name(s) e.g. form-control fa-users',
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_form_wizard_form` (`form_id`),
  KEY `FK_form_wizard_user_created` (`created_by_id`),
  KEY `FK_form_wizard_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_job`
--

CREATE TABLE IF NOT EXISTS `pms_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK2__pms_staff_pms_job` (`created_by_id`),
  KEY `FK3__pms_staff_pms_job` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_job_manufacturing_link`
--

CREATE TABLE IF NOT EXISTS `pms_job_manufacturing_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pms_manufacturing_route_id` int(11) NOT NULL,
  `pms_job_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK1__pms_job_manufacturing_link` (`pms_manufacturing_route_id`),
  KEY `FK2__pms_job_manufacturing_link` (`pms_job_id`),
  KEY `FK3__pms_job_manufacturing_link` (`created_by_id`),
  KEY `FK4__pms_job_manufacturing_link` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_job_process`
--

CREATE TABLE IF NOT EXISTS `pms_job_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_job_process_user_created` (`created_by_id`),
  KEY `FK_job_process_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_job_process_form`
--

CREATE TABLE IF NOT EXISTS `pms_job_process_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_process_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_job_process_form_job_process` (`job_process_id`),
  KEY `FK_job_process_form_form` (`form_id`),
  KEY `FK_job_process_form_user_created` (`created_by_id`),
  KEY `FK_job_process_form_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_job_type`
--

CREATE TABLE IF NOT EXISTS `pms_job_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK2__pms_staff_pms_job_type` (`created_by_id`),
  KEY `FK3__pms_staff_pms_job_type` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_job_type_form`
--

CREATE TABLE IF NOT EXISTS `pms_job_type_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `job_type_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK1__pms_job_type_form` (`form_id`),
  KEY `FK2__pms_job_type_form` (`job_type_id`),
  KEY `FK3__pms_job_type_form` (`created_by_id`),
  KEY `FK4__pms_job_type_form` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_location`
--

CREATE TABLE IF NOT EXISTS `pms_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_location_user_created` (`created_by_id`),
  KEY `FK_location_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_machine`
--

CREATE TABLE IF NOT EXISTS `pms_machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `control_number` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  `parent_machine_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_machine_user_created` (`created_by_id`),
  KEY `FK_machine_user_updated` (`updated_by_id`),
  KEY `FK_machine_parent` (`parent_machine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_machine_detail`
--

CREATE TABLE IF NOT EXISTS `pms_machine_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machine_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_machine_detail_machine` (`machine_id`),
  KEY `FK_machine_detail_user_created` (`created_by_id`),
  KEY `FK_machine_detail_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_machine_form`
--

CREATE TABLE IF NOT EXISTS `pms_machine_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machine_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_machine_form_machine` (`machine_id`),
  KEY `FK_machine_form_form` (`form_id`),
  KEY `FK_machine_form_user_created` (`created_by_id`),
  KEY `FK_machine_form_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_machine_location`
--

CREATE TABLE IF NOT EXISTS `pms_machine_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machine_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_machine_location_machine` (`machine_id`),
  KEY `FK_machine_location_location` (`location_id`),
  KEY `FK_machine_location_user_created` (`created_by_id`),
  KEY `FK_machine_location_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_machine_operation`
--

CREATE TABLE IF NOT EXISTS `pms_machine_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machine_id` int(11) NOT NULL,
  `operation_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_machine_operation_machine` (`machine_id`),
  KEY `FK_machine_operation_operation` (`operation_id`),
  KEY `FK_machine_operation_user_created` (`created_by_id`),
  KEY `FK_machine_operation_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_manufacturing_route`
--

CREATE TABLE IF NOT EXISTS `pms_manufacturing_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_type_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `sort_order` int(11) DEFAULT NULL,
  `preferred_sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK1__pms_manufacturing_route` (`job_type_id`),
  KEY `FK2__pms_manufacturing_route` (`created_by_id`),
  KEY `FK3__pms_manufacturing_route` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_operation`
--

CREATE TABLE IF NOT EXISTS `pms_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_operation_user_created` (`created_by_id`),
  KEY `FK_operation_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_operation_form`
--

CREATE TABLE IF NOT EXISTS `pms_operation_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operation_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_operation_form_operation` (`operation_id`),
  KEY `FK_operation_form_form` (`form_id`),
  KEY `FK_operation_form_user_created` (`created_by_id`),
  KEY `FK_operation_form_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_process_operation_link`
--

CREATE TABLE IF NOT EXISTS `pms_process_operation_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pms_operation_id` int(11) NOT NULL,
  `pms_job_process_id` int(11) NOT NULL,
  `sequence_order` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK1__pms_process_operation_link` (`pms_operation_id`),
  KEY `FK2__pms_process_operation_link` (`pms_job_process_id`),
  KEY `FK3__pms_process_operation_link` (`created_by_id`),
  KEY `FK4__pms_process_operation_link` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_qc_checklist`
--

CREATE TABLE IF NOT EXISTS `pms_qc_checklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qc_check_group_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `guidelines` text,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_qc_checklist_qc_check_group` (`qc_check_group_id`),
  KEY `FK_qc_checklist_user_created` (`created_by_id`),
  KEY `FK_qc_checklist_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_qc_checklist_form`
--

CREATE TABLE IF NOT EXISTS `pms_qc_checklist_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qc_check_group_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_qc_checklist_form_qc_check_group` (`qc_check_group_id`),
  KEY `FK_qc_checklist_form_form` (`form_id`),
  KEY `FK_qc_checklist_form_user_created` (`created_by_id`),
  KEY `FK_qc_checklist_form_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_qc_check_group`
--

CREATE TABLE IF NOT EXISTS `pms_qc_check_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `created_by_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_id` int(11) DEFAULT NULL,
  `is_soft_deleted` int(1) DEFAULT '0' COMMENT '0 = not solf_deleted; 1 = solf_deleted',
  `status` int(1) DEFAULT '1' COMMENT '0 = inactive; 1 = active',
  PRIMARY KEY (`id`),
  KEY `FK_qc_check_group_user_created` (`created_by_id`),
  KEY `FK_qc_check_group_user_updated` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_type_process_link`
--

CREATE TABLE IF NOT EXISTS `pms_type_process_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pms_manufacturing_route_id` int(11) NOT NULL,
  `pms_job_process_id` int(11) NOT NULL,
  `sequence_order` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `solf_deleted` smallint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not solf_deleted, 1 = solf_deleted',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Active, 1 = Active',
  PRIMARY KEY (`id`),
  KEY `FK1__pms_type_process_link` (`pms_manufacturing_route_id`),
  KEY `FK2__pms_type_process_link` (`pms_job_process_id`),
  KEY `FK3__pms_type_process_link` (`created_by_id`),
  KEY `FK4__pms_type_process_link` (`updated_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pms_form`
--
ALTER TABLE `pms_form`
  ADD CONSTRAINT `FK2__pms_staff_pms_form` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__pms_staff_pms_form` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pms_form_field`
--
ALTER TABLE `pms_form_field`
  ADD CONSTRAINT `FK1__pms_staff_pms_form_field` FOREIGN KEY (`form_id`) REFERENCES `pms_form` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2__pms_staff_pms_form_field` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3__pms_staff_pms_form_field` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pms_form_field_properties`
--
ALTER TABLE `pms_form_field_properties`
  ADD CONSTRAINT `FK1__pms_staff_pms_form_field_properties` FOREIGN KEY (`form_field_id`) REFERENCES `pms_form_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2__pms_staff_pms_form_field_properties` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3__pms_staff_pms_form_field_properties` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pms_form_style`
--
ALTER TABLE `pms_form_style`
  ADD CONSTRAINT `FK_form_style_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_form_style_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_form_wizard`
--
ALTER TABLE `pms_form_wizard`
  ADD CONSTRAINT `FK_form_wizard_form` FOREIGN KEY (`form_id`) REFERENCES `pms_form` (`id`),
  ADD CONSTRAINT `FK_form_wizard_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_form_wizard_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_job`
--
ALTER TABLE `pms_job`
  ADD CONSTRAINT `FK2__pms_staff_pms_job` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3__pms_staff_pms_job` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pms_job_manufacturing_link`
--
ALTER TABLE `pms_job_manufacturing_link`
  ADD CONSTRAINT `FK1__pms_job_manufacturing_link` FOREIGN KEY (`pms_manufacturing_route_id`) REFERENCES `pms_manufacturing_route` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2__pms_job_manufacturing_link` FOREIGN KEY (`pms_job_id`) REFERENCES `pms_job` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3__pms_job_manufacturing_link` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4__pms_job_manufacturing_link` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pms_job_process`
--
ALTER TABLE `pms_job_process`
  ADD CONSTRAINT `FK_job_process_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_job_process_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_job_process_form`
--
ALTER TABLE `pms_job_process_form`
  ADD CONSTRAINT `FK_job_process_form_form` FOREIGN KEY (`form_id`) REFERENCES `pms_form` (`id`),
  ADD CONSTRAINT `FK_job_process_form_job_process` FOREIGN KEY (`job_process_id`) REFERENCES `pms_job_process` (`id`),
  ADD CONSTRAINT `FK_job_process_form_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_job_process_form_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_job_type`
--
ALTER TABLE `pms_job_type`
  ADD CONSTRAINT `FK2__pms_staff_pms_job_type` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3__pms_staff_pms_job_type` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pms_job_type_form`
--
ALTER TABLE `pms_job_type_form`
  ADD CONSTRAINT `FK1__pms_job_type_form` FOREIGN KEY (`form_id`) REFERENCES `pms_form` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2__pms_job_type_form` FOREIGN KEY (`job_type_id`) REFERENCES `pms_job_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3__pms_job_type_form` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4__pms_job_type_form` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pms_location`
--
ALTER TABLE `pms_location`
  ADD CONSTRAINT `FK_location_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_location_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_machine`
--
ALTER TABLE `pms_machine`
  ADD CONSTRAINT `FK_machine_parent` FOREIGN KEY (`parent_machine_id`) REFERENCES `pms_machine` (`id`),
  ADD CONSTRAINT `FK_machine_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_machine_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_machine_detail`
--
ALTER TABLE `pms_machine_detail`
  ADD CONSTRAINT `FK_machine_detail_machine` FOREIGN KEY (`machine_id`) REFERENCES `pms_machine` (`id`),
  ADD CONSTRAINT `FK_machine_detail_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_machine_detail_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_machine_form`
--
ALTER TABLE `pms_machine_form`
  ADD CONSTRAINT `FK_machine_form_form` FOREIGN KEY (`form_id`) REFERENCES `pms_form` (`id`),
  ADD CONSTRAINT `FK_machine_form_machine` FOREIGN KEY (`machine_id`) REFERENCES `pms_machine` (`id`),
  ADD CONSTRAINT `FK_machine_form_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_machine_form_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_machine_location`
--
ALTER TABLE `pms_machine_location`
  ADD CONSTRAINT `FK_machine_location_location` FOREIGN KEY (`location_id`) REFERENCES `pms_location` (`id`),
  ADD CONSTRAINT `FK_machine_location_machine` FOREIGN KEY (`machine_id`) REFERENCES `pms_machine` (`id`),
  ADD CONSTRAINT `FK_machine_location_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_machine_location_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_machine_operation`
--
ALTER TABLE `pms_machine_operation`
  ADD CONSTRAINT `FK_machine_operation_machine` FOREIGN KEY (`machine_id`) REFERENCES `pms_machine` (`id`),
  ADD CONSTRAINT `FK_machine_operation_operation` FOREIGN KEY (`operation_id`) REFERENCES `pms_operation` (`id`),
  ADD CONSTRAINT `FK_machine_operation_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_machine_operation_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_manufacturing_route`
--
ALTER TABLE `pms_manufacturing_route`
  ADD CONSTRAINT `FK1__pms_manufacturing_route` FOREIGN KEY (`job_type_id`) REFERENCES `pms_job_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2__pms_manufacturing_route` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3__pms_manufacturing_route` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pms_operation`
--
ALTER TABLE `pms_operation`
  ADD CONSTRAINT `FK_operation_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_operation_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_operation_form`
--
ALTER TABLE `pms_operation_form`
  ADD CONSTRAINT `FK_operation_form_form` FOREIGN KEY (`form_id`) REFERENCES `pms_form` (`id`),
  ADD CONSTRAINT `FK_operation_form_operation` FOREIGN KEY (`operation_id`) REFERENCES `pms_operation` (`id`),
  ADD CONSTRAINT `FK_operation_form_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_operation_form_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_process_operation_link`
--
ALTER TABLE `pms_process_operation_link`
  ADD CONSTRAINT `FK1__pms_process_operation_link` FOREIGN KEY (`pms_operation_id`) REFERENCES `pms_operation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2__pms_process_operation_link` FOREIGN KEY (`pms_job_process_id`) REFERENCES `pms_job_process` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3__pms_process_operation_link` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4__pms_process_operation_link` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pms_qc_checklist`
--
ALTER TABLE `pms_qc_checklist`
  ADD CONSTRAINT `FK_qc_checklist_qc_check_group` FOREIGN KEY (`qc_check_group_id`) REFERENCES `pms_qc_check_group` (`id`),
  ADD CONSTRAINT `FK_qc_checklist_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_qc_checklist_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_qc_checklist_form`
--
ALTER TABLE `pms_qc_checklist_form`
  ADD CONSTRAINT `FK_qc_checklist_form_form` FOREIGN KEY (`form_id`) REFERENCES `pms_form` (`id`),
  ADD CONSTRAINT `FK_qc_checklist_form_qc_check_group` FOREIGN KEY (`qc_check_group_id`) REFERENCES `pms_qc_check_group` (`id`),
  ADD CONSTRAINT `FK_qc_checklist_form_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_qc_checklist_form_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_qc_check_group`
--
ALTER TABLE `pms_qc_check_group`
  ADD CONSTRAINT `FK_qc_check_group_user_created` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`),
  ADD CONSTRAINT `FK_qc_check_group_user_updated` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`);

--
-- Constraints for table `pms_type_process_link`
--
ALTER TABLE `pms_type_process_link`
  ADD CONSTRAINT `FK1__pms_type_process_link` FOREIGN KEY (`pms_manufacturing_route_id`) REFERENCES `pms_manufacturing_route` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2__pms_type_process_link` FOREIGN KEY (`pms_job_process_id`) REFERENCES `pms_job_process` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3__pms_type_process_link` FOREIGN KEY (`created_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4__pms_type_process_link` FOREIGN KEY (`updated_by_id`) REFERENCES `pms_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- +migrate Down
